###############################################################################################
####                                     Build SETTINGS                                    #### 
MAKE_REVISION                  := 1.00s
###############################################################################################



##################################### POST 17.1 ###############################################
LEGACY_EMU_FLAGS := -march=emulator -v -g -legacy-emulator -emulator-channel-depth-model=strict
LEGACY_RUN_FLAGS := env CL_CONTEXT_EMULATOR_DEVICE_INTELFPGA=1
FAST_EMU_FLAGS := -march=emulator -v -g 
FAST_RUN_FLAGS := env CL_CONFIG_CPU_EMULATE_DEVICES=1
FPGA_FLAGS := -v 
PROFILE_FLAGS := -v -profile 
###############################################################################################

# Compilation flags
ifeq ($(DEBUG),1)
CXXFLAGS += -g 
else
CXXFLAGS += -O2 
endif

# Compiler
CXX = g++

# Target
TARGET := host
SOURCES_DIR := host/src
TARGET_DIR := bin

# Directories
INC_DIRS := host/inc ../common/inc include /home/gil/gsl/include cl/include
LIB_DIRS := /home/gil/gsl/lib

# Files
INCS := $(wildcard include/*.h ../common/inc/*.hpp cl/include/*.h)
SRCS := $(wildcard  src/opencl.cpp src/fp2bin.c  src/generic_fifo.c  src/inverse.c  src/lin_alg_gen.c   src/opt_pricer_refactored.c  ../common/src/*.cpp ../common/src/AOCLUtils/*.cpp)
LIBS := gsl gslcblas m quadmath
#LIBS := rt pthread


OCL_SRC := cl/optPricerTop.cl
OCL_KERNELS := cl/*.cl cl/include/*.h
OCL_TARGET := optPricerTop

#executable Files arguments
MAIN_ARGS := ../opt_pricer.config

include Makefile_$(MAKE_REVISION).app


