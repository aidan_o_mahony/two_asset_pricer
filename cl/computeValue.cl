#include "include/optPricer.h"


#pragma OPENCL EXTENSION cl_intel_channels : enable
__attribute__((max_global_work_dim(0)))
__attribute__((num_compute_units(COMPUTE_DEPTH,1)))
__attribute__((autorun))
kernel void computeValue ( )
{	

	float mult_coeff[NUM_BRANCHES];	
	type_payout payout[NUM_BRANCHES];
	type_payout euPayout;
	bool  control = 0;
	int processingUnit = get_compute_id(0);

	for (unsigned char i = 0; i < NUM_BRANCHES; i++) {
		mult_coeff[i] = read_channel_intel(mult_coeff_channel[processingUnit]);
		//printf ("Read mult_coeff: %f \n", mult_coeff[i]);
	}

	while (control == 0) {
		float sumP  =  0;
		#pragma unroll NUM_BRANCHES
		for (unsigned short i = 0; i < NUM_BRANCHES; i++) {
			payout[i] = read_channel_intel(euPricerInChannel[processingUnit][i]);
			sumP  +=  (payout[i].payout * mult_coeff[i]);
		}
		euPayout =  payout[0];
		if (euPayout.finished == 0)  euPayout.payout = sumP;
		euPayout.endLine = payout[NUM_BRANCHES-1].endLine;


		//printf ("Payout: %f PUID: %d Finished: %d, Last %d\n", euPayout.payout, processingUnit, euPayout.finished, euPayout.last);
		// COMPUTE Payout.
		write_channel_intel(euPricerOutChannel[processingUnit+1], euPayout);
		if (euPayout.last || euPayout.finished) {
			break;
		}
	}
}

