#include "include/optPricer.h"


#pragma OPENCL EXTENSION cl_intel_channels : enable
__attribute__((max_global_work_dim(0)))
__attribute__((num_compute_units(COMPUTE_DEPTH,1)))
__attribute__((autorun))
kernel void dataSorter ( )
{	

	bool control = 0;
	bool finished = 0;
	bool last = 0;
	unsigned short innerControl;
	int  processingUnit = get_compute_id(0);
	bool writeChannel[NUM_BRANCHES];
	unsigned short currentTreeDepth;
	unsigned short newTreeDepth;
	unsigned short i;
	unsigned short __attribute__((register)) block[NUM_BRANCHES];
	type_payout payout;

	unsigned short depthChannel;
	while (control == 0) {
		currentTreeDepth = read_channel_intel(currentTreeDepthChannel[processingUnit]);
		//printf ("--------------------TREE DEPTH ----- %d, \n" ,currentTreeDepth); 
		newTreeDepth = 0;
		if (currentTreeDepth == 1) {
			last = 1;
		} else if (currentTreeDepth == 0) {
			finished = 1;
		} else {
			newTreeDepth = currentTreeDepth - 1;
			finished = 0;
			last = 0;
		}
		innerControl = 1;
		if ((last != 1) || (finished != 1)) {
			write_channel_intel(currentTreeDepthChannel[processingUnit+ 1], newTreeDepth);	
		}
		//innerControl = currentTreeDepth;
		block[0] = 1;		
		block[1] = 0;
		block[2] = 0;
		while (innerControl != 0) {
			payout = read_channel_intel(euPricerOutChannel[processingUnit]);
			//printf ("Payout: %f,   %d, %d, %d ,PUID: %d, last: %d Finished: %d\n" ,payout.payout, block[0] ,block[1] ,block[2], processingUnit, last , finished); 
			if ((last == 1) && (finished == 1)) break;
			payout.last     = last;
			payout.finished = finished;
			#pragma unroll NUM_BRANCHES
			for (i = 0; i < NUM_BRANCHES; i++) {
				if (block[i] != 0 || last == 1 || finished == 1) {
					write_channel_intel(euPricerInChannel[processingUnit][i], payout);
				}
			}
			if ((last ==1) || (finished == 1)) break;
			block[2] = block[1];
			if (block[NUM_BRANCHES-2] == 0) {
				if (block[0] == 0) {
					innerControl = 0;
				}
				block[1] = block[0];
				++block[0];
				if (block[0] == currentTreeDepth) {
					block[0] = 0;
				} 
			} else {
				--block[1];
			}
		}
	}
}

