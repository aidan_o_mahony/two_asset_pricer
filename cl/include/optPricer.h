#ifndef OPT_PRICER_KERNELS_H
#define OPT_PRICER_KERNELS_H

#define NUM_ASSETS 2
#define NUM_BRANCHES 3
#define MAX_TREE_DEPTH 512
#define COMPUTE_DEPTH 1

typedef struct type_stockPriceTree {  float         stockPrice; 
				      unsigned short numberOfBranches;
				      unsigned short currentBranch;
				      unsigned short returnBranch;
				      unsigned short index;
		                   } type_stockPriceTree;

typedef struct type_value          {  float         value; 
				      bool           finished;
		                   } type_value;

typedef struct type_payout         {  float         payout; 
				      bool           startLine;
		                      bool           endLine;
		                      bool           finished;
				      bool           last;
		                   } type_payout;

typedef struct type_optionValue    {  float         optionValue; 
				      bool           finished;
		                   } type_optionValue;


// Channels to pass data from host
/*global*/	channel float  init_step_channel[NUM_ASSETS]         __attribute__((depth(NUM_ASSETS)));
/*global*/	channel float  strike_channel                        __attribute__((depth(NUM_ASSETS)));
/*global*/	channel float  init_step_next_channel[NUM_ASSETS]    __attribute__((depth(16)));
/*global*/	channel float  mult_coeff_channel[COMPUTE_DEPTH]     __attribute__((depth(NUM_BRANCHES)));
/*global*/	channel float  init_shares_channel[NUM_ASSETS]       __attribute__((depth(16)));
/*global*/	channel unsigned short max_min_channel                __attribute__((depth(1)));
/*global*/	channel float cash_channel                           __attribute__((depth(1)));
/*global*/	channel unsigned short call_put_channel               __attribute__((depth(1)));
/*global*/	channel unsigned short tree_depth_channel[NUM_ASSETS] __attribute__((depth(1)));



/*global*/	channel unsigned short currentTreeDepthChannel[COMPUTE_DEPTH+1] __attribute__((depth(1)));
/*global*/	channel unsigned short muxTreeDepthChannel                      __attribute__((depth(1)));
/*global*/	channel unsigned short currentTreeDepthChannelReturn            __attribute__((depth(1)));

//StockPrice calculation Channels Out
/*global*/ channel type_value   stockValueChannel[NUM_ASSETS]     __attribute__((depth(256)));

//First Payout calculation Channels Out
/*global*/ channel type_payout  payoutChannel                     __attribute__((depth(256)));

//MUX Channels (2 in 1 out)
/*global*/ channel type_payout  payoutChannelReturn  __attribute__((depth(131072)));
/*global*/ channel type_payout  payoutChannelOut     __attribute__((depth(256)));
/*global*/ channel bool         finishedChannel      __attribute__((depth(1)));


// Eu pricer Channels.
/*global*/ channel type_payout  euPricerInChannel[COMPUTE_DEPTH][NUM_BRANCHES]  __attribute__((depth(MAX_TREE_DEPTH)));
/*global*/ channel type_payout  euPricerOutChannel[COMPUTE_DEPTH+1]             __attribute__((depth(256)));

/*global*/ channel type_payout  finalOptionPrice[COMPUTE_DEPTH]     __attribute__((depth(1)));



#endif

