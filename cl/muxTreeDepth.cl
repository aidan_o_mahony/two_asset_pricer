#include "include/optPricer.h"

#pragma OPENCL EXTENSION cl_intel_channels : enable
//__attribute__((max_global_work_dim(0)))
//__attribute__((autorun))
kernel void muxTreeDepth ( )
{	

	type_payout payout;
	bool control = 0;
	bool finished = 0;
	unsigned short treeDepth;

	while (finished == 0) {
		//printf("..control %d..\n", control);
		if (control == 0) {
			treeDepth = read_channel_intel(muxTreeDepthChannel);
			control   = 1;
		}
		else {
			treeDepth = read_channel_intel(currentTreeDepthChannel[COMPUTE_DEPTH]);
		}
		//printf("..depth %d..\n", treeDepth);
		write_channel_intel(currentTreeDepthChannel[0], treeDepth);
		if (treeDepth == 0) {
			control = 0;
			break;
		}
	}
	//printf("......tree mux exit........................\n");
}

