#include "include/optPricer.h"

#pragma OPENCL EXTENSION cl_intel_channels : enable
//__attribute__((max_global_work_dim(0)))
//__attribute__((autorun))
kernel void payoutKernel ( )
{	

float __attribute__((register)) strike_prob_vect[NUM_ASSETS];
unsigned short max_min;
float cash;
unsigned short call_put;

//global unsigned short treeDepth;

	//float __attribute__((register)) strike_prob_vect[NUM_ASSETS];
	//nsigned short max_min = *mx_mn;
	//float         cash = *csh;
	type_value  stockValue[NUM_ASSETS];
	type_payout payout;
	bool  control = 0;


	for (unsigned char i = 0; i < NUM_ASSETS; i++) {
		strike_prob_vect[i] = read_channel_intel(strike_channel);
		//printf ("Read Strike: %f \n", strike_prob_vect[i]);
	}
	max_min  = read_channel_intel(max_min_channel);
	cash     = read_channel_intel(cash_channel);
	call_put = read_channel_intel(call_put_channel);
	payout.finished  = 0;
	payout.startLine = 1;
	payout.endLine   = 0;
	payout.last      = 0;
	while (control == 0) {
		float maxTemp = 0;
		float minTemp = 10000000;
		float fTemp;
		float delta;
		#pragma unroll NUM_ASSETS
		for (unsigned char i = 0; i < NUM_ASSETS; i++) {
			stockValue[i] = read_channel_intel(stockValueChannel[i]);
			if (call_put == 1) {
				delta  =  stockValue[i].value - strike_prob_vect[i];
			} else {
				delta  =  strike_prob_vect[i] - stockValue[i].value;
			}
			if (maxTemp < delta) { maxTemp = delta;} 
			if (minTemp > delta) { minTemp = delta;} 			
			if ((max_min==1 && call_put ==1) || (max_min==0 && call_put == 0)) {
				fTemp = maxTemp;
			} else {
				fTemp = minTemp;
			}
		}
		float maxVal = fTemp;
		if (fTemp < cash) {
			maxVal = cash;
		}
		control        = stockValue[0].finished;
		payout.endLine = control;
		payout.payout  = maxVal;
		//printf ("----------------------------------------Payout: %f Start: %d  End: %d\n", maxVal, payout.startLine, payout.endLine);
		// COMPUTE Payout.
		write_channel_intel(payoutChannel, payout);
		payout.startLine = 0;
	}
}

