
__kernel void producer ( __global float* restrict init_step1, // init_step[0]     // 0
                         __global float* restrict init_step2, // init_step[1]     // 1
                         __global float* restrict strikevect, // strike           // 2
                         __global float* restrict istepnext,  // init_step_next   // 3
                         __global float* restrict mcoeef,     // mult_coeff       // 4
                         __global float* restrict ishares,    // init shares      // 5
                         __global unsigned short* restrict mx_mn,                  // 6
                         __global float * restrict csh,                           // 7
                         __global unsigned short* restrict cput,                   // 6
			 unsigned short treeDepth)
{


	// Need to understand the ordering.
	// strike[NUM_ASSETS]         = {strikevect[0],strikevect[1]           };
	// init_step_next[NUM_ASSETS] = {istepnext[0], istepnext[1]            };
	// mult_coeff[NUM_BRANCHES]   = {mcoeef[2],    mcoeef[1],    mcoeef[0] };
	// init_shares[NUM_ASSETS]    = {ishares[0],   ishares[1]              };
	//  init_step[0][0] = init_step1[0]; init_step[0][1] = init_step1[1];
	//  init_step[1][0] = init_step2[0]; init_step[1][1] = init_step2[1];

	unsigned char i;
	unsigned char j;
	unsigned char k;
	for (i = 0; i < NUM_ASSETS; i++) { // NEED to make this cleaner.  Pass single array instead of multiple arrays 
		write_channel_intel(init_step_channel[0],init_step1[i]);
		write_channel_intel(init_step_channel[1],init_step2[i]);
	}

	for (i = 0; i < NUM_ASSETS; i++) {
		write_channel_intel(strike_channel,strikevect[i]);
	}


	for (i = 0; i < NUM_ASSETS; i++) {
		//init_step_next[i] = istepnext[i];
	}

	for (i = 0; i < NUM_ASSETS; i++) {
		write_channel_intel(init_shares_channel[i],ishares[i]);
	}

	for (i = 0; i < NUM_BRANCHES; i++) {
		for (k = 0; k < COMPUTE_DEPTH; k++) {
			write_channel_intel(mult_coeff_channel[k],mcoeef[i]);
		}
	}

	for (i = 0; i < NUM_ASSETS; i++) {
		write_channel_intel(tree_depth_channel[i],treeDepth);
	}
	write_channel_intel(max_min_channel, *mx_mn);
	write_channel_intel(cash_channel, *csh);
	write_channel_intel(call_put_channel, *cput);
	write_channel_intel(muxTreeDepthChannel, (treeDepth+1));

	bool waiting = read_channel_intel(finishedChannel);
}

