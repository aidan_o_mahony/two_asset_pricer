#include "include/optPricer.h"

#pragma OPENCL EXTENSION cl_intel_channels : enable
//__attribute__((max_global_work_dim(0)))
//__attribute__((autorun))
kernel void returnBuffer ( )
{	

	type_payout payout;
	bool finished = 0;
	unsigned short currentTreeDepth;

	while (finished == 0) {
		payout           = read_channel_intel(euPricerOutChannel[COMPUTE_DEPTH]);
		write_channel_intel(payoutChannelReturn, payout);
		finished = payout.last | payout.finished;
	}
	//printf(".............return Buffer exit........................\n");
}

