#include "include/optPricer.h"


#pragma OPENCL EXTENSION cl_intel_channels : enable
__attribute__((max_global_work_dim(0)))
__attribute__((num_compute_units(NUM_BRANCHES-1,1)))
__attribute__((autorun))
kernel void  stockPrice  ( )
{

	// Variables
	//printf ("stock Price Entry \n");
	
	float __attribute__((register)) init_step[NUM_BRANCHES-1];
	float __attribute__((register)) init_shares;
	unsigned short treeDepth;

	//init_step[0] = init_step1[0]; init_step[1] = init_step1[1];
	//float __attribute__((register)) init_shares = {ishares[0]};
	type_stockPriceTree __attribute__((register)) stockTree[NUM_BRANCHES-1];
	type_value stockValue;
	bool finished;  
	finished = 0;
	unsigned short currentIndex = 0;
	unsigned short nextIndex = 0;
	unsigned short tempIndex;
	bool moveSideways = 0;
	int proessingUnit = get_compute_id(0);
	unsigned char i;
	for (i = 0; i < NUM_BRANCHES-1; i++) {
		init_step[i] = read_channel_intel(init_step_channel[proessingUnit]);
		//printf ("init_step: %f \n",init_step[i]);
	}
	init_shares = read_channel_intel(init_shares_channel[proessingUnit]);
	//printf ("init_shares: %f \n",init_shares);
	treeDepth   = read_channel_intel(tree_depth_channel[proessingUnit]);
	//printf ("treeDepth: %d \n",treeDepth);

	stockValue.finished = 0;
	stockValue.value = init_shares;

	write_channel_intel (stockValueChannel[proessingUnit], stockValue);
	stockTree[currentIndex].stockPrice = init_shares;
	stockTree[0].numberOfBranches = treeDepth;
	stockTree[0].currentBranch    = 1;
	stockTree[0].returnBranch     = 1;
	stockTree[0].index            = 0;
	while (1) {
		if (moveSideways) {
			stockTree[currentIndex].stockPrice =stockTree[currentIndex-1].stockPrice * init_step[currentIndex];
		} else {
			stockTree[currentIndex].stockPrice *= init_step[currentIndex];
		}
		stockValue.value = stockTree[currentIndex].stockPrice;
		write_channel_intel (stockValueChannel[proessingUnit], stockValue);
		//printf ("Unit: %d, stock value: %f \n",proessingUnit, stockValue.value);
		if (finished) break;
		moveSideways = 1;
		nextIndex = currentIndex + 1;
		if (currentIndex < NUM_BRANCHES -2) { 
			stockTree[nextIndex].numberOfBranches = stockTree[currentIndex].currentBranch;
			stockTree[nextIndex].currentBranch    = 1;
			stockTree[nextIndex].index            = stockTree[currentIndex].currentBranch + 1;
			if ((stockTree[nextIndex].numberOfBranches == 1) || 
                            (nextIndex == (NUM_BRANCHES-2)) ||
                            (stockTree[nextIndex].numberOfBranches == stockTree[nextIndex].currentBranch)){
				stockTree[nextIndex].returnBranch = stockTree[currentIndex].returnBranch;
			} else {
				stockTree[nextIndex].returnBranch = nextIndex + 1;
			}
			currentIndex = nextIndex;
		} 

		else if (stockTree[currentIndex].numberOfBranches != stockTree[currentIndex].currentBranch) { 
			++stockTree[currentIndex].currentBranch;	
		moveSideways = 0;
			if (stockTree[currentIndex].currentBranch == (treeDepth) && (stockTree[currentIndex].numberOfBranches == treeDepth)) {
				finished = 1;
				stockValue.finished = 1;
			}
		} 

                else { 
			moveSideways = 0;
			if (stockTree[currentIndex].currentBranch == (treeDepth)) {
				finished = 1;
				stockValue.finished = 1;
			} else {
				currentIndex = stockTree[currentIndex].returnBranch - 1;
				++stockTree[currentIndex].currentBranch;
				if (currentIndex > 0 && ( stockTree[currentIndex].numberOfBranches == stockTree[currentIndex].currentBranch )) {
					stockTree[currentIndex].returnBranch = stockTree[currentIndex - 1].returnBranch;
				}
			}
		}
	}
}


