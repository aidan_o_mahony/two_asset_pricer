#########################################################
#														#
# ".config" file for opt_pricer  program:					#
# ----------------------------------					#
#	o	Lines with #'s are ignored.						#
#	o	Format must be preserved (just change values).	#
#	o	This file is loaded when the programm is run.	#
#														#
#########################################################


#########################################################
#														#
# Eurpean AND OR American option type
#	1 campute;  0 do not compute													#
#It is possible to have both simulateusly (not yet)
#########################################################
OPT_EUROPEAN            1
OPT_AMERICAN            0


#########################################################
#														#
# CALL or PUT options
#   1 compute CAll ption, 0 computes PUT options
####################################################

CALL_PUT		1
########################################################
#														#
# Number of layers
#########################################################
NUM_LAYERS              32


#########################################################
#														#
# Dimension
#########################################################
NUM_DIM	                2
###########################################################
## Share value at current time/origin
###########################################################
INIT_SHARE_VALUE  40,40


####################################################
#Covariance Matrix
# It must be a	COMMA SEPARATED list of EXACTLY NUM_DIM*(NUM_DIM+1) elemens
#Less elements will results in failure, extra elements will be ignored
#the elements are read as lx2rx,top2bottom	 					
# ie: SIGMA  1,2,3,4=| 1 2|
#            	     | 3 4|
#######################################
SIGMA 0.04,0.0,0.0,0.04

#########################################################
#  RATE : as in the value calc function
#########################################################R
T	    0.5833333333
RATE        0

#########################################################
#########################################################
#														#
# Payoff Function related parameters:										#
# --------------										#
#	o	STRIKE  : Of use only for multistrike 		#							#
#	o	MAX_MIN : pay on max or min
#	o	CASH    : Of use only for +cash options type
#########################################################

#########################################################
#														#
#  STRIKE:  
# It must be a	COMMA SEPARATED list of EXACTLY NUM_DIM elemens
#Less elements will results in failure, extra elements will be ignored
# If all 0 then Better/Worst of payoff function is implemented
# If all = then Max/Min of payoff function is implemented
# If not = then Multistrike payoff function is implemented
#########################################################

STRIKE          45,45


#########################################################
#														#
# MAX_MIN : 1 MAX/BEST,  0 MIN/WORST of
#########################################################
MAX_MIN        1


#########################################################
#														#
# CASH : if != 0 the "+cash" (max(Fp,CASH) is implemented
#########################################################
CASH        0


#########################################################
#														#
# VERBOSE:  if != 0 print verbose output
#########################################################
VERBOSE  1

#########################################################
# Expected result (delete if needed to run program)
#########################################################
#RESULT=1.54955502775205999910
