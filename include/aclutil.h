#ifndef ACLUTIL_H
#define ACLUTIL_H

// Allocate and free memory aligned to value that's good for
// Altera OpenCL performance.
void *acl_aligned_malloc (size_t size);
void  acl_aligned_free (void *ptr);
unsigned char* load_file(const char* filename,size_t*size_ret);
void display_device_info( cl_device_id device );
//bool setCwdToExeDir();
#endif
