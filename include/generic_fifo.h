
#ifndef _FIFO_H
#define	_FIFO_H
/* ========== */
/* Structures */
/* ========== */

/* fifo_node_struct */
typedef struct fifo_node_struct {
  void *element; 
  struct fifo_node_struct *next;
} fifo_node;

/* generic fifo struct */
typedef struct generic_fifo_struct {
  unsigned long size;
  fifo_node *first ; 
  fifo_node *last ; 
} generic_fifo;

/* =================== */
/* Function prototypes */
/* =================== */
generic_fifo *fifo_gen();
fifo_node *fifo_node_gen(void *element);
void fifo_push(generic_fifo *fifo,void *element);
void *fifo_pop(generic_fifo *fifo);
void fifo_free(generic_fifo *fifo);
unsigned long fifo_size(generic_fifo *fifo);
void assert_mem(void *ptr_to_new_mem) ;


#endif
