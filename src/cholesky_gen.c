 
 #include <config.h>
 #include <stdlib.h>
 #include <gsl/gsl_math.h>
 #include <gsl/gsl_vector.h>
 #include <gsl/gsl_matrix.h>
 #include <gsl/gsl_linalg.h>
 #include "opt_pricer.h"

 #define REAL float_type
static inline
float_type
quiet_sqrt (float_type x)
     /* avoids runtime error, for checking matrix for positive definiteness */
{
  return (x >= 0) ? SQRT_FUNC(x) : GSL_NAN;
}


/* Cholesky Decomposition
*
* Copyright (C) 2000 Thomas Walter
*
* 03 May 2000: Modified for GSL by Brian Gough
* 29 Jul 2005: Additions by Gerard Jungman
* Copyright (C) 2000,2001, 2002, 2003, 2005, 2007 Brian Gough, Gerard Jungman
*
* This is free software; you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation; either version 3, or (at your option) any
* later version.
*
* This source is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
* for more details.
*/

/*
* Cholesky decomposition of a symmetrix positive definite matrix.
* This is useful to solve the matrix arising in
* periodic cubic splines
* approximating splines
*
* This algorithm does:
* A = L * L'
* with
* L := lower left triangle matrix
* L' := the transposed form of L.
*
*/


int
linalg_cholesky_decomp_gen (float_type **A, size_t rows, size_t cols)
{
  const size_t M = rows;
  const size_t N = cols;

  if (M != N)
    {
      GSL_ERROR("cholesky decomposition requires square matrix", GSL_ENOTSQR);
    }
  else
    {
      size_t i,j,k;
      int status = 0;

      /* Do the first 2 rows explicitly. It is simple, and faster. And
* one can return if the matrix has only 1 or 2 rows.
*/

      float_type A_00 = A[0][0];
      
      float_type L_00 = quiet_sqrt(A_00);
      
      if (A_00 <= 0)
        {
          status = GSL_EDOM ;
        }

      A[0][0]=L_00;
  
      if (M > 1)
        {
          float_type A_10 = A[1][0];
          float_type A_11 = A[1][1];          
          float_type L_10 = A_10 / L_00;
          float_type diag = A_11 - L_10 * L_10;
          float_type L_11 = quiet_sqrt(diag);
          
          if (diag <= 0)
            {
              status = GSL_EDOM;
            }

          A[1][0]= L_10;
          A[1][1]= L_11;
        }
      
      for (k = 2; k < M; k++)
        {
          float_type A_kk = A[k][k];
          
          for (i = 0; i < k; i++)
            {
              float_type sum = 0;

              float_type A_ki = A[k][i];
              float_type A_ii = A[i][i];

//              gsl_vector_view ci = gsl_matrix_row (A, i);
//              gsl_vector_view ck = gsl_matrix_row (A, k);

              if (i > 0) {
		
//                gsl_vector_view di = gsl_vector_subvector(&ci.vector, 0, i);
//                gsl_vector_view dk = gsl_vector_subvector(&ck.vector, 0, i);                
//                gsl_blas_ddot (&di.vector, &dk.vector, &sum);
		for (int ir=0;ir<i;ir++){
		  for (int ic=0;ic<i;ic++){
		    sum +=A[i][ir]*A[k][ic];
		  }
		  }
              }

              A_ki = (A_ki - sum) / A_ii;
              A[k][i]=A_ki;
            }

          {
//            gsl_vector_view ck = gsl_matrix_row (A, k);
//            gsl_vector_view dk = gsl_vector_subvector (&ck.vector, 0, k);
            
            float_type sum = 0;//gsl_blas_dnrm2 (&dk.vector);
	    for (int ki=0;ki<k;ki++){
	      sum+=POW_FUNC(A[k][ki],2);
	    }
	    sum=quiet_sqrt(sum);

            float_type diag = A_kk - sum * sum;
            float_type L_kk = quiet_sqrt(diag);
            
            if (diag <= 0)
              {
                status = GSL_EDOM;
              }
            
            A[k][k]=L_kk;
          }
        }

      /* Now copy the transposed lower triangle to the upper triangle,
* the diagonal is common.
*/
      
      for (i = 1; i < M; i++)
        {
          for (j = 0; j < i; j++)
            {
              float_type A_ij = A[i][j];
              A[j][i]= A_ij;
            }
        }
      
      if (status == GSL_EDOM)
        {
          GSL_ERROR ("matrix must be positive definite", GSL_EDOM);
        }
      
      return GSL_SUCCESS;
    }
}


/* linalg/hh.c
  * 
  * Copyright (C) 1996, 1997, 1998, 1999, 2000 Gerard Jungman, Brian Gough
  * 
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 2 of the License, or (at
  * your option) any later version.
  * 
  * This program is distributed in the hope that it will be useful, but
  * WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  * General Public License for more details.
  * 
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
  */
 
 /* Author:  G. Jungman */
 
 
 int
 linalg_HH_svx_gen (float_type **A, const ulong rowA,const ulong colA, float_type *x, const ulong dimX)
 {
   if (colA > rowA)
     {
       /* System is underdetermined. */
 
       GSL_ERROR ("System is underdetermined", GSL_EINVAL);
     }
   else if (rowA != dimX)
     {
       GSL_ERROR ("matrix and vector sizes must be equal", GSL_EBADLEN);
     }
   else
     {
       const size_t N = colA;
       const size_t M = rowA;
       size_t i, j, k;
       REAL *d = (REAL *) malloc (N * sizeof (REAL));
 
       if (d == 0)
         {
           GSL_ERROR ("could not allocate memory for workspace", GSL_ENOMEM);
         }
 
       /* Perform Householder transformation. */
 
       for (i = 0; i < N; i++)
         {
           const REAL aii = A[i][i];
           REAL alpha;
           REAL f;
           REAL ak;
           REAL max_norm = 0.0;
           REAL r = 0.0;
 
           for (k = i; k < M; k++)
             {
               REAL aki = A[k][i];
               r += aki * aki;
             }
 
           if (r == 0.0)
             {
               /* Rank of matrix is less than size1. */
               free (d);
               GSL_ERROR ("matrix is rank deficient", GSL_ESING);
             }
 
           alpha = sqrt (r) * GSL_SIGN (aii);
 
           ak = 1.0 / (r + alpha * aii);
           A[i][i]=aii + alpha;
 
           d[i] = -alpha;
 
           for (k = i + 1; k < N; k++)
             {
               REAL norm = 0.0;
               f = 0.0;
               for (j = i; j < M; j++)
                 {
                   REAL ajk =A[j][k];
                   REAL aji =A[j][i];
                   norm += ajk * ajk;
                   f += ajk * aji;
                 }
               max_norm = MAX_FUNC(max_norm, norm);
 
               f *= ak;
 
               for (j = i; j < M; j++)
                 {
                   REAL ajk = A[j][k];
                   REAL aji = A[j][i];
                   A[j][k]=ajk - f * aji;
                 }
             }
 
           if (ABS_FUNC(alpha) < 2.0 * GSL_DBL_EPSILON * SQRT_FUNC (max_norm))
             {
               /* Apparent singularity. */
               free (d);
               GSL_ERROR("apparent singularity detected", GSL_ESING);
             }
 
           /* Perform update of RHS. */
 
           f = 0.0;
           for (j = i; j < M; j++)
             {
               f += x[j] * A[j][i];
             }
           f *= ak;
           for (j = i; j < M; j++)
             {
               REAL xj  = x[j];
               REAL aji = A[j][i];
               x[j]= xj - f * aji;
             }
         }
 
       /* Perform back-substitution. */
 
       for (i = N; i > 0 && i--;)
         {
           REAL xi = x[i];
           REAL sum = 0.0;
           for (k = i + 1; k < N; k++)
             {
               sum += A[i][k] * x[k];
             }
 
           x[i]= (xi - sum) / d[i];
         }
 
       free (d);
       return GSL_SUCCESS;
     }
 }
/* matrix multiply for any type */
//R=AB
void matrix_mult_gemm(float_type **R, float_type **A, float_type **B, const ulong rowR,const ulong colR, const ulong rowA,const ulong colA, const ulong rowB,const ulong colB) {
  ulong i		= 0;
  ulong j		= 0;
  ulong h		= 0;
  
  /* stupid checks */
  if (!A) {
    fprintf(stderr, "WARNING:  matrix A does not exist\n");
    return ;
  } else if (!B) {
    fprintf(stderr, "WARNING:  matrix B does not exist\n");
    return ;
  } else if(!R) {
    fprintf(stderr, "WARNING:  matrix R does not exist\n");
    return ;
  }
 
  /* check dimensions */
  if(colA != rowB || rowA != rowR || colR != colB) {
    fprintf(stderr, "ERROR:  matrix dimensions do not agree\n");
    printf("	    [%lu x %lu]=[%lu x %lu].[%lu x %lu]\n", rowR, colR, rowA, colA, rowB, colB);
    exit(EXIT_FAILURE);
  }
 
  /* multiply */
  for(j=0 ; j<rowA ; j++) {
    for(i=0 ; i<colB ; i++) {
      R[j][i] = 0; 
      for(h=0 ; h<colA ; h++) {
	R[j][i] += A[j][h] * B[h][i];
      }
    }
  } 
  return;
}
