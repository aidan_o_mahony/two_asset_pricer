#include "generic_fifo.h"
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>
#include <math.h>
#include <string.h>

fifo_node *fifo_node_gen(void *element){
  fifo_node *new_node=NULL;
  new_node = (fifo_node*)malloc(sizeof(fifo_node));
  assert_mem(new_node);
  new_node->element=element;
  new_node->next=NULL;
 
}
generic_fifo *fifo_gen(){
  generic_fifo *fifo=NULL;
  fifo = (generic_fifo*)malloc(sizeof(generic_fifo));
  assert_mem(fifo);
  fifo->size=0;
  fifo->first=NULL;
  fifo->last=NULL;
}
void fifo_push(generic_fifo *fifo,void *element){
  fifo_node *new_node=NULL;
  new_node=fifo_node_gen(element);
  if (fifo->size==0  ){
    fifo->first=new_node;
    fifo->last=new_node;   
  } else{
    fifo->last->next=new_node; 
    fifo->last=new_node; 
  }
  fifo->size++;
}

void *fifo_pop(generic_fifo *fifo){
  void *return_element=NULL;
  fifo_node *old_node=NULL;
  if (fifo->size==0){
    fprintf(stderr,"ERROR: EMPTY FIFO");
    exit(EXIT_FAILURE);
  }
  old_node=fifo->first;
  return_element=old_node->element;
  fifo->first=old_node->next;
  fifo->size--;
  free(old_node);
  return return_element;  
}
void fifo_free(generic_fifo *fifo){
}
unsigned long fifo_size(generic_fifo *fifo){
  return fifo->size;
}
