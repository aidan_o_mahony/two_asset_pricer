/* Header file */
#define DO_FPGA
#include "opt_pricer.h"
#include "lin_alg_gen.h"
#include "inverse.h"
//#include "utils.h"
#include "opencl.h"
#include <time.h>
#ifdef DO_FPGA
#include "aocl_utils.h"
#include "aclutil.h"
using namespace aocl_utils;
#endif


///////////////////////////////////////////////////////////
////                         MACROS                      //
///////////////////////////////////////////////////////////
//#define DBG_VHDL
//#define INIT_TB_GEN

#define TOP_TB_GEN
#define DEBUG
//#define DEBUG_2ND_MOM
#define SCND_MOM_MATCH
//#define PLOT_CONSTELLATION

/* ==== */
/* main */
/* ==== */

/*global vars*/
ulong payoff_num_calls=0;
ulong payoff_num_nz=0;
#ifdef PLOT_CONSTELLATION
FILE *fid_cost=NULL;
#endif
/*global vars*/
#define NUM_LAYER 10
#define STATUS(x) status = x; if(status < 0) printf("ERROR at line %d\n",__LINE__)


/******************************************************************************
 * Global OpenCL device/queue/kernel handles
 * ***************************************************************************/
cl_platform_id   platform           = NULL;
cl_device_id     device             = NULL;

cl_command_queue queue[5]     = {NULL, NULL, NULL, NULL, NULL};
cl_kernel        kernel[5]    = {NULL, NULL, NULL, NULL, NULL};
cl_program       program = NULL;

cl_context       context = NULL;
cl_mem           input = NULL;
cl_mem           output = NULL;

static const char* aocx_name = "optPricerTop.aocx";
static const char* kernel_names[5] = {"producer", "payoutKernel", "returnBuffer", "muxPayout", "muxTreeDepth"};


int num_layers = 0;
int size_of_array = 0;
int num_partition_calls = 0;
#define CHECK_MAX_SIZE(x) size_of_array = fifo_size(x) > size_of_array ? fifo_size(x) : size_of_array
void printPlatform()
{
      int i, j;
    char* value;
    size_t valueSize;
    cl_uint platformCount;
    cl_platform_id* platforms;
    cl_uint deviceCount;
    cl_device_id* devices;
    cl_uint maxComputeUnits;

    // get all platforms
    clGetPlatformIDs(0, NULL, &platformCount);
    platforms = (cl_platform_id*) malloc(sizeof(cl_platform_id) * platformCount);
    clGetPlatformIDs(platformCount, platforms, NULL);

    for (i = 0; i < platformCount; i++) {

        // get all devices
        clGetDeviceIDs(platforms[i], CL_DEVICE_TYPE_CPU, 0, NULL, &deviceCount);
        devices = (cl_device_id*) malloc(sizeof(cl_device_id) * deviceCount);
        clGetDeviceIDs(platforms[i], CL_DEVICE_TYPE_CPU, deviceCount, devices, NULL);

        // for each device print critical attributes
        for (j = 0; j < deviceCount; j++) {

            // print device name
            clGetDeviceInfo(devices[j], CL_DEVICE_NAME, 0, NULL, &valueSize);
            value = (char*) malloc(valueSize);
            clGetDeviceInfo(devices[j], CL_DEVICE_NAME, valueSize, value, NULL);
            printf("%d. Device: %s\n", j+1, value);
            free(value);

            // print hardware device version
            clGetDeviceInfo(devices[j], CL_DEVICE_VERSION, 0, NULL, &valueSize);
            value = (char*) malloc(valueSize);
            clGetDeviceInfo(devices[j], CL_DEVICE_VERSION, valueSize, value, NULL);
            printf(" %d.%d Hardware version: %s\n", j+1, 1, value);
            free(value);

            // print software driver version
            clGetDeviceInfo(devices[j], CL_DRIVER_VERSION, 0, NULL, &valueSize);
            value = (char*) malloc(valueSize);
            clGetDeviceInfo(devices[j], CL_DRIVER_VERSION, valueSize, value, NULL);
            printf(" %d.%d Software version: %s\n", j+1, 2, value);
            free(value);

            // print c version supported by compiler for device
            clGetDeviceInfo(devices[j], CL_DEVICE_OPENCL_C_VERSION, 0, NULL, &valueSize);
            value = (char*) malloc(valueSize);
            clGetDeviceInfo(devices[j], CL_DEVICE_OPENCL_C_VERSION, valueSize, value, NULL);
            printf(" %d.%d OpenCL C version: %s\n", j+1, 3, value);
            free(value);

            // print parallel compute units
            clGetDeviceInfo(devices[j], CL_DEVICE_MAX_COMPUTE_UNITS,
                    sizeof(maxComputeUnits), &maxComputeUnits, NULL);
            printf(" %d.%d Parallel compute units: %d\n", j+1, 4, maxComputeUnits);

        }

        free(devices);

    }
}
#ifdef DO_FPGA
bool initFPGA()
{
        cl_int status;
        int err;
        printf("Starting initFPGA\n");
        if(!setCwdToExeDir()) {
                return false;
        }
        printf("Lets see if we can find the intel platform\n");
        // Get the OpenCL platform.
        platform = findPlatform("Intel");
        if(platform == NULL) {
                printf("ERROR: Unable to find Altera OpenCL platform.\n");
                return false;
        }

        //User-visible output - Platform information
#if 0
        {
                char char_buffer[STRING_BUFFER_LEN]; 
                printf("Querying platform for info:\n");
                printf("==========================\n");
                clGetPlatformInfo(platform, CL_PLATFORM_NAME, STRING_BUFFER_LEN, char_buffer, NULL);
                printf("%-40s = %s\n", "CL_PLATFORM_NAME", char_buffer);
                clGetPlatformInfo(platform, CL_PLATFORM_VENDOR, STRING_BUFFER_LEN, char_buffer, NULL);
                printf("%-40s = %s\n", "CL_PLATFORM_VENDOR ", char_buffer);
                clGetPlatformInfo(platform, CL_PLATFORM_VERSION, STRING_BUFFER_LEN, char_buffer, NULL);
                printf("%-40s = %s\n\n", "CL_PLATFORM_VERSION ", char_buffer);
        }
#endif
        // Query the available OpenCL devices.
        scoped_array<cl_device_id> devices;
        cl_uint num_devices;

        devices.reset(getDevices(platform, CL_DEVICE_TYPE_ALL, &num_devices));

        // We'll just use the first device.
        device = devices[0];
        //display_device_info(device);

        std::string binary_file = getBoardBinaryFile("optPricerTop", device);                                        
        printf("file = %s\n",binary_file.c_str());
        context = clCreateContext(NULL, 1, &device, NULL, NULL, &status);                           // create context
        if(status < 0) printf("ERROR at line %d\n",__LINE__);
        printf("Context created\n");
	for (int i = 0; i < 5; i++) {
		queue[i] = clCreateCommandQueue(context, device, CL_QUEUE_PROFILING_ENABLE, &status);
		if(status < 0) printf("ERROR at line %d\n",__LINE__);
	}
        printf("Command qs created\n");
        program = createProgramFromBinary(context, binary_file.c_str(), &device, 1);             // create program
        printf("Program created\n");
        status     = clBuildProgram(program, 0, NULL, "", NULL, NULL);
        if(status < 0) printf("ERROR at line %d\n",__LINE__);
        printf("About to write buffer\n");
	for (int i = 0; i < 5; i++) {
	        kernel[i] = clCreateKernel(program, kernel_names[i], &status);
	}
        printf("Completed initFPGA\n");
        return true;
}
#endif

double doKernel(opt_price1_params *opp)
{
        cl_mem cl_init_share, cl_strike, cl_init_step1, cl_init_step2, cl_init_step_next, cl_mult_coeff, cl_shares_coeff, cl_mx_min, output, cl_cash, cl_call_put;
        cl_int status;
        int err = 0;
        cl_event kernel_event[5];
        float opt_val;
	unsigned short  treeDepth   = opp->num_layers;   // TREE DEPTH
       // MEMORY
        cl_init_step1      = clCreateBuffer(context, CL_MEM_READ_ONLY, 2*2*sizeof(float), NULL, &err);
        cl_init_step2      = clCreateBuffer(context, CL_MEM_READ_ONLY, 2*sizeof(float), NULL, &err);
        cl_strike          = clCreateBuffer(context, CL_MEM_READ_ONLY, 2*sizeof(float), NULL, &err);
        cl_init_step_next  = clCreateBuffer(context, CL_MEM_READ_ONLY, 2*sizeof(float), NULL, &err);
        cl_mult_coeff      = clCreateBuffer(context, CL_MEM_READ_ONLY, 3*sizeof(float), NULL, &err);
        cl_init_share      = clCreateBuffer(context, CL_MEM_READ_ONLY, 2*sizeof(float), NULL, &err);
        cl_mx_min          = clCreateBuffer(context, CL_MEM_READ_ONLY, 1*sizeof(unsigned short), NULL, &err);
        cl_cash            = clCreateBuffer(context, CL_MEM_READ_ONLY, 1*sizeof(float), NULL, &err);
        cl_call_put        = clCreateBuffer(context, CL_MEM_READ_ONLY, 1*sizeof(unsigned short), NULL, &err);
        output             = clCreateBuffer(context, CL_MEM_WRITE_ONLY, 1*sizeof(float), NULL, &err);
        double nanoSeconds; 
        STATUS(clSetKernelArg(kernel[0],0,sizeof(cl_mem),&cl_init_step1));
        STATUS(clSetKernelArg(kernel[0],1,sizeof(cl_mem),&cl_init_step2));
        STATUS(clSetKernelArg(kernel[0],2,sizeof(cl_mem),&cl_strike));
        STATUS(clSetKernelArg(kernel[0],3,sizeof(cl_mem),&cl_init_step_next));
        STATUS(clSetKernelArg(kernel[0],4,sizeof(cl_mem),&cl_mult_coeff));
        STATUS(clSetKernelArg(kernel[0],5,sizeof(cl_mem),&cl_init_share));
        STATUS(clSetKernelArg(kernel[0],6,sizeof(cl_mem),&cl_mx_min));
        STATUS(clSetKernelArg(kernel[0],7,sizeof(cl_mem),&cl_cash));
        STATUS(clSetKernelArg(kernel[0],8,sizeof(cl_mem),&cl_call_put));
        STATUS(clSetKernelArg(kernel[0],9,sizeof(treeDepth), &treeDepth));

        STATUS(clSetKernelArg(kernel[3],0,sizeof(cl_mem),&output));

        STATUS(clEnqueueWriteBuffer(queue[0],cl_init_step1,CL_TRUE,0,4*sizeof(float),opp->init_step[0],0,NULL,NULL));
        STATUS(clEnqueueWriteBuffer(queue[0],cl_init_step2,CL_TRUE,0,2*sizeof(float),opp->init_step[1],0,NULL,NULL));
        STATUS(clEnqueueWriteBuffer(queue[0],cl_strike,CL_TRUE,0,2*sizeof(float),opp->strike,0,NULL,NULL));
        STATUS(clEnqueueWriteBuffer(queue[0],cl_init_step_next,CL_TRUE,0,2*sizeof(float),opp->init_step_next,0,NULL,NULL));
        STATUS(clEnqueueWriteBuffer(queue[0],cl_mult_coeff,CL_TRUE,0,3*sizeof(float),opp->mult_coeff,0,NULL,NULL));
        STATUS(clEnqueueWriteBuffer(queue[0],cl_init_share,CL_TRUE,0,2*sizeof(float),opp->init_share,0,NULL,NULL));
        STATUS(clEnqueueWriteBuffer(queue[0],cl_mx_min,CL_TRUE,0,1*sizeof(unsigned short),&(opp->max_min),0,NULL,NULL));
        STATUS(clEnqueueWriteBuffer(queue[0],cl_cash,CL_TRUE,0,1*sizeof(float),&(opp->cash),0,NULL,NULL));
        STATUS(clEnqueueWriteBuffer(queue[0],cl_call_put,CL_TRUE,0,1*sizeof(unsigned short),&(opp->call_put),0,NULL,NULL));

 
        size_t globalItemSize = 1;
        size_t localItemSize = 64; // globalItemSize has to be a multiple of localItemSize. 1024/64 = 16 

	struct timeval t1, t2;
	double elapsedTimeCPU;


        for(int k = 0; k < 100; k++) {
		err = clEnqueueTask(queue[0], kernel[0], 0, NULL, NULL);    
		err = clEnqueueTask(queue[2], kernel[2], 0, NULL, NULL);    
		err = clEnqueueTask(queue[3], kernel[3], 0, NULL, &kernel_event[3]);    
		err = clEnqueueTask(queue[4], kernel[4], 0, NULL, NULL);    
		err = clEnqueueTask(queue[1], kernel[1], 0, NULL, NULL);    
		//clWaitForEvents(1,&kernel_event);
		//err = clFinish(queue[3]);
		// Read from device back to host.
		err = clEnqueueReadBuffer(queue[3], output, CL_TRUE, 0, 1 * sizeof(float), &opt_val, 0, NULL, NULL);

		/************************************************************************************************
		 * Calculate execution time
		 * *********************************************************************************************/
		cl_ulong time_start, time_end;
		clGetEventProfilingInfo(kernel_event[3], CL_PROFILING_COMMAND_START, sizeof(time_start), &time_start, NULL);
		clGetEventProfilingInfo(kernel_event[3], CL_PROFILING_COMMAND_END, sizeof(time_end), &time_end, NULL);
		printf("Option Price %d= ",k);
    		prinf_float_type(" ",opt_val,PRNT_PREC);
		printf("\n");
		if(k != 0) nanoSeconds += time_end - time_start;
        }
        printf("Kernel took %f microseconds\n",nanoSeconds/(1000.0*99));
        printf("Option Price = %f\n", opt_val);

        return opt_val;
}


int main(int argc, char **argv) {
    /* ========= */
    /* Variables */
    /* ========= */
    FILE *fid						= NULL;
    ulong i,j							= 0;
    opt_price_params opp;
    opt_price1_params opp1;
    struct timespec time_start, time_end;
    double elapsed_time				= 0.0;
    struct tm *program_finish_tm	= NULL;
    struct tm *program_start_tm		= NULL;
    time_t program_finish			= 0;
    time_t program_start			= 0;
    char temp_string[STR_MAX]		= {""};

    ushort num_dim=0;

    char config_file[FILENAME_MAX]	= {""};

    //FIFOs DATA structures
    generic_fifo *fifo_shares=NULL;   //contaiing arrays of float
    generic_fifo **fifo_value_vect=NULL;  //arrays of fifos containing single floats
    generic_fifo *fifo_partis=NULL;   //cpntaining arrays of ushort
    float_type *value_tmp=NULL;
    float_type *old_val=NULL;
    float_type new_val_float;    
    ushort div_index=0;
    float_type *new_shares=NULL;
    float_type *old_shares=NULL;
    float_type dt;
    ushort *old_part=NULL;
    ushort *new_part=NULL;

    config_set *conf_set=NULL;


    float_type  **d_matrix=NULL;
    float_type *q_vector=NULL;


#ifdef PLOT_CONSTELLATION
    fid_cost=fopen("constellation.csv","w");
    fprintf(fid_cost,"S1,S2,Fpayoff\n");
#endif
    /* ================= */
    /* Examine arguments */
    /* ================= */
    printf("prec_used=%d\n",sizeof(float_type));;
    if(argc == 1) {
        strcpy(config_file, CONFIG_FILE);

    } else if(argc == 2) {
        strcpy(config_file, argv[1]);

    } else {
        usage_opt_pricer();

    }


    /* ===== */
    /* Intro */
    /* ===== */
    printf("=============================\n");
    printf("   Option Pricer Program     \n");
    printf("=============================\n");

    time(&program_start);
    program_start_tm = localtime(&program_start);
    strftime(temp_string, STR_MAX-1, "%A %d/%B/%Y at %H:%M:%S\0", program_start_tm);
    printf("Program started on %s\n", temp_string);


    /* ======================= */
    /* Load configuration file */
    /* ======================= */
    printf("Loading configuration info...\n");

    fid = fopen(config_file,"r");
    if(fid == NULL) {
        fprintf(stderr, "\nERROR: Could not open <%s> file.",config_file);
        fprintf(stderr, "\n       Need to specify valid configuration file.\n");
        exit(EXIT_FAILURE);
    }

    conf_set=(config_set *)malloc(sizeof(config_set));
    assert_mem(conf_set);
    read_config(fid,conf_set,config_file);

    fclose(fid);
    dt=(float_type)(conf_set->T/conf_set->NUM_LAYERS);
    printf("dt = %f\n",dt);
    num_dim=conf_set->NUM_DIM;

    //  d_matrix=gsl_matrix_calloc(num_dim,num_dim+1);
    d_matrix=(float_type **)malloc(num_dim*sizeof(float_type *));
    assert_mem(d_matrix);
    for (i=0;i<num_dim;i++){
        d_matrix[i]=(float_type *)malloc((num_dim+1)*sizeof(float_type ));
        assert_mem(d_matrix[i]);
    }



    q_vector=(float_type *)malloc((num_dim+1)*sizeof(float_type));
    assert_mem(q_vector);
    compute_d_and_q(conf_set->sigma_matrix, d_matrix, q_vector, conf_set->RATE,  dt,num_dim,conf_set->T);
#ifdef DEBUG_2ND_MOM
    print_float_type_matrix(d_matrix,num_dim,num_dim+1,"\nd_new=[");
    print_float_type_vector(q_vector,num_dim+1,"\nq_new=[");
#endif

    float_type sum,sum2=0;
    printf("\n");
    //copy values to conf_set
    for(i=0;i<num_dim;i++){
        for(j=0;j<num_dim+1;j++){
            conf_set->share_mult_vect[i][j]=d_matrix[i][j];
        }
    }

    //normilize q   //usefull only for __float128
    for (i=0;i<num_dim+1;i++){
        //     prinf_float_type("\nprev",(float_type)gsl_vector_get(q_vector,i),PRNT_PREC);
        sum+=q_vector[i];
    }
    //no nomilization
    //      sum=1;
    //prinf_float_type("\nprev sum",sum,PRNT_PREC);
    for (i=0;i<num_dim+1;i++){
        conf_set->eu_prob_vect[i]=q_vector[i]/sum;
        //    prinf_float_type("\npost",conf_set->eu_prob_vect[i],PRNT_PREC);
        sum2+=conf_set->eu_prob_vect[i];
    }



    if(conf_set->VERBOSE){
        printf("\nSHARE_MULT_FACT	 =");
        for(i=0;i<num_dim;i++){
            for(j=0;j<num_dim+1;j++){
                printf("%.4f,",conf_set->share_mult_vect[i][j]);
            }
            printf("\n\t\t"); 
        }

        printf("\nEU_PROB_FACT	 =");
        for(i=0;i<num_dim+1;i++){
            printf("%.4f,",conf_set->eu_prob_vect[i]);
        }
    }

    /* =========================== */
    /* Create and initialize fifos */
    /* =========================== */
    printf("\nInitializing fifos...\n");
    fifo_value_vect=(generic_fifo **)malloc((num_dim+1)*sizeof(generic_fifo*));
    assert_mem(fifo_value_vect);
    for (i=0; i<num_dim+1;i++){
        fifo_value_vect[i]=fifo_gen();
    }

    fifo_shares=fifo_gen(); 
    fifo_partis=fifo_gen(); 

    old_val=(float_type *)calloc(num_dim+1,sizeof(float_type));
    assert_mem(old_val);
    init_fifos(fifo_shares,fifo_value_vect,fifo_partis,conf_set, &opp);

    /*============================*/
    /*      MAIN LOOP             */
    /*============================*/
    while (fifo_size(fifo_partis)!=0){ //I'l stop when reached the root
        /*pop the elements from the fifos*/
        old_part=(ushort *)fifo_pop(fifo_partis);
        old_shares=(float_type *)fifo_pop(fifo_shares);
        for(i=0;i<num_dim+1;i++){
            old_val[i]=*((float_type *)fifo_pop(fifo_value_vect[i]));
        }
        //    print_partition(old_part,num_dim);
#ifdef DEBUG
        printf("\n+++++++++++++++++++++++++++FIFO_partis_size=%d",fifo_size(fifo_partis));
        printf("\n+++++++++++++++++++++++++++FIFO_shares_size=%d",fifo_size(fifo_shares));
        for(i=0;i<num_dim+1;i++){
            printf("\n+++++++++++++++++++++++++++FIFO_values_size[%d]=%d",i,fifo_size(fifo_value_vect[i]));
        }
        print_partition(old_part,num_dim,"old");
        printf("-");

        printf("\nHEX PRICEX =%x\n",*(unsigned int*)&(old_val[0]));     
        for (i=0; i<num_dim+1;i++){
            printf("\n v%d=%.18Qf-->HEX=%x",i,old_val[i],*(unsigned int*)&(old_val[i]));
        }
        printf("+");
        print_shares(old_shares,num_dim,"old");
        fflush(NULL);
#endif

        //Compute new partitions and store
        if (old_part[0]!=0){  
            new_part=old_part;
            new_part[0]--;
#ifdef DEBUG
            print_partition(new_part,num_dim,"new");
#endif
            if (new_part[0]!=0){
                fifo_push(fifo_partis,new_part);      
            }
        }
        //COMPUTE new share and store

        new_shares=old_shares;
#ifdef DBG_VHDL
        float aa;
        print_partition(new_part,num_dim,"new");
        for(i=0;i<num_dim;i++){
            aa=new_shares[i];
            printf("\nshare[%d]=%.4f=%x ",i,aa,*(unsigned int*)&(aa));   
            aa=1/conf_set->share_mult_vect[i][0];
            printf(",coeff[%d]=%.4f=%x ",i,aa,*(unsigned int*)&(aa));   
            aa=old_shares[i]/conf_set->share_mult_vect[i][0];
            printf("\nnext_share[%d]=%.4f=%x ",i,aa,*(unsigned int*)&(aa));  
        }

#endif
#ifndef DEBUG 
        if (new_part[0]!=0){  
            div_index=0;
            for(i=0;i<num_dim;i++){
                new_shares[i]=old_shares[i]/(float_type)conf_set->share_mult_vect[i][div_index]; 
            }
            fifo_push(fifo_shares,new_shares);
        }
#endif 

#ifdef DEBUG 
        div_index=0;
        for(i=0;i<num_dim;i++){
            new_shares[i]=old_shares[i]/(float_type)conf_set->share_mult_vect[i][div_index]; 
        }
        if (new_part[0]!=0){      
            fifo_push(fifo_shares,new_shares);
        }

        print_shares(new_shares,num_dim,"new");
        printf("\nSx=%x",*(unsigned int*)&(new_shares[0]));
        printf("\nSy=%x",*(unsigned int*)&(new_shares[1]));
#endif 


        //USE  new shares to compute new value
        new_val_float=compute_value(old_val,new_shares,conf_set);
#ifdef DEBUG
        prinf_float_type("\nNEW_VAL",new_val_float,PRNT_PREC);
        prinf_float_type_hex("--> HEX",new_val_float);

        //    printf("=%.18f ->HEX=%x",new_val_float,*(unsigned int*)&(new_val_float));
#endif
#ifdef DBG_VHDL
        aa=old_val[0];
        printf("\n-old_val=%.18f=%x ",aa,*(unsigned int*)&(aa));   
        aa=new_val_float;
        printf("\n-new_val=%.18f=%x ",aa,*(unsigned int*)&(aa));   
#endif

        //STORE new values in all dims if appropriate
        for (int i=0;i<num_dim+1;i++){
            if (new_part[i]!=0){
                value_tmp=(float_type *)calloc(1,sizeof(float_type*));  //replicate the value returned from prev function so to be copied into fifos
                *value_tmp=new_val_float;
                fifo_push(fifo_value_vect[i],(void *)(value_tmp));
                value_tmp=NULL;
            }
        }

    } //Main loop

    printf("\n====================================\n");
    prinf_float_type(" OPTION PRICE =",new_val_float,PRNT_PREC);
    printf("\n");
    prinf_float_type_hex(" HEX PRICE =",new_val_float);
    printf("\n");
    printf("N=%3d ",conf_set->NUM_LAYERS);
    printf("Tot num of calls to Fpayoff= %lu\n",payoff_num_calls);
    printf("      Tot num of non zero Fpayoff= %lu\n",payoff_num_nz);
    printf("      Perc.   of non zero Fpayoff= %.2f\n",payoff_num_nz/(double)payoff_num_calls*100);
    printf("====================================\n");


    /* ================ */
    /* Cleanup and exit */
    /* ================ */
    /* ================ */
    /* Cleanup and exit */
    /* ================ */
    printf("\nExiting...\n");
    fflush(NULL); 
    /* Print time */
    clock_gettime(CLOCK_MONOTONIC, &time_end);
    printf("Timer start = %d %d\n",time_start.tv_sec, time_start.tv_nsec);
    printf("Timer end   = %d %d\n",time_end.tv_sec, time_end.tv_nsec);
    printf("DIFF = %f usec\n",(time_end.tv_nsec-time_start.tv_nsec)/1000.0);

    printf("Done.\n");

    printf("******************************************************************************************************************\n");
    printf("******************************************************************************************************************\n");
    printf("******************************************************************************************************************\n");
    printf("************************************** RUNNING  OPENCL ***********************************************************\n");
    printf("******************************************************************************************************************\n");
    printf("******************************************************************************************************************\n");
    printf("******************************************************************************************************************\n");
    printf("******************************************************************************************************************\n");

    initFPGA();
    opp1.max_min             = opp.max_min;
    opp1.num_layers          = opp.num_layers;
    opp1.init_share[0]       = (float)opp.init_share[0];
    opp1.init_share[1]       = (float)opp.init_share[1];
    opp1.strike[0]           = (float)opp.strike[0];
    opp1.strike[1]           = (float)opp.strike[1];
    opp1.cash                = (float)opp.cash;
    opp1.init_step[0][0]     = (float)opp.init_step[0][0];
    opp1.init_step[0][1]     = (float)opp.init_step[0][1];
    opp1.init_step[1][0]     = (float)opp.init_step[1][0];
    opp1.init_step[1][1]     = (float)opp.init_step[1][1];
    opp1.init_step_next[0]   = (float)opp.init_step_next[0];
    opp1.init_step_next[1]   = (float)opp.init_step_next[1];
    opp1.mult_coeff[0]       = (float)opp.mult_coeff[0];
    opp1.mult_coeff[1]       = (float)opp.mult_coeff[1];
    opp1.mult_coeff[2]       = (float)opp.mult_coeff[2];
    opp1.shares_coeff[0]     = (float)opp.shares_coeff[0];
    opp1.shares_coeff[1]     = (float)opp.shares_coeff[1];
    opp1.call_put            = opp.call_put;
   
    double temp = doKernel(&opp1);
    printf("Option value = %f\n",temp);


    return 0;

}
/* usage_ldpc_sim */
void usage_opt_pricer() {
    fprintf(stderr,"================================\n");
    fprintf(stderr,"OPTION PRICER calculator:\n");
    fprintf(stderr,"================================\n");
    fprintf(stderr,"Usage:   opt_pricer  [<config_file>]\n");
    fprintf(stderr,"Option:  <config_file>  Use <config_file> configuration file.\n");
    fprintf(stderr,"                        Default is 'opt_pricer.config'\n\n");
    exit(EXIT_FAILURE);
}
/* assert_mem */
/* Checks to see if memory has been allocated */
void assert_mem(void *ptr_to_new_mem) {
    if(ptr_to_new_mem == NULL) {
        fprintf(stderr, "\nERROR: Could not allocate new memory.\n");
        exit(EXIT_FAILURE);
    } else {
        return;
    }
}
void init_fifos( generic_fifo *fifo_shares,generic_fifo **fifo_value_vect,generic_fifo *fifo_partis,config_set *conf_set, opt_price_params *opp){
    ushort *init_partition=NULL;
    float_type *init_share_d0=NULL;
    float_type init_value=0;
    int i=0;
    ushort num_dim=conf_set->NUM_DIM;
    ushort num_layers=conf_set->NUM_LAYERS;
    float_type **share_mult_vect=conf_set->share_mult_vect;
    float_type *value_opt_curr_vect=NULL;
    float_type *zeros_val=NULL;
    float_type pow_res=0;

    init_share_d0=(float_type *)calloc(num_dim,sizeof(float_type));
    assert_mem(init_share_d0);
    printf("num_dim = %d\n",num_dim);
    for(i=0; i<num_dim;i++){
        pow_res=(float_type)POW_FUNC((float_type)(share_mult_vect[i][0]),(float_type)num_layers);
        init_share_d0[i]=conf_set->init_share_value_vect[i]*pow_res;
        printf("conf_set->init_share_value_vect[%d] = %f\n",i,conf_set->init_share_value_vect[i]);
        printf("pow_res = %f\n",pow_res);
        printf("init_share_d0[%d] = %f\n",i,init_share_d0[i]);
    }
    init_partition=(ushort*)calloc(num_dim+1,sizeof(ushort));
    assert_mem(init_partition);
    init_partition[0]=num_layers;
    zeros_val=(float_type *)calloc(num_dim,sizeof(float_type));
    assert_mem(zeros_val);
    init_value=compute_payoff(init_share_d0,conf_set);
    free(zeros_val);
    fifo_push(fifo_shares,(void*)init_share_d0); 
    fifo_push(fifo_partis,(void*)init_partition); 
    value_opt_curr_vect=(float_type *)malloc((num_dim+1)*sizeof(float_type));
    assert_mem(value_opt_curr_vect);

    /* for (int i=0;i<num_dim+1;i++){
       value_opt_curr_vect[0]=init_value;
       fifo_push(fifo_value_vect[i],(void *)&(value_opt_curr_vect[i]));
       }*/
    value_opt_curr_vect[0]=init_value;
    fifo_push(fifo_value_vect[0],(void *)&(value_opt_curr_vect[0]));

#ifdef DEBUG
    print_shares(init_share_d0,num_dim,"init");
    printf("\n");
    printf("Final layer partitions=[");
    for (i=0;i<num_dim+1;i++){
        printf("%d,",init_partition[i]);
    }
    printf("]\n");
#endif

    //Compute all partitions in final layer
    gen_partitions_rec(init_partition,0,init_share_d0,fifo_partis,fifo_value_vect,fifo_shares,conf_set); 

#ifdef TOP_TB_GEN
    FILE *fid_tb=NULL;
    float_type aa;
    char binString[PRECISION+2];

    fid_tb=fopen("opt_pricer_TB_data.dat","w");
    //  fprintf(fid_tb,"%d\n",((conf_set->MAX_MIN)+1)%2);
    fprintf(fid_tb,"%d\n",conf_set->MAX_MIN);
    opp->call_put = conf_set->CALL_PUT;
    opp->max_min = conf_set->MAX_MIN;
    opp->num_layers = conf_set->NUM_LAYERS;
    fprintf(fid_tb,"%s\n",byte_to_binary(conf_set->NUM_LAYERS)); //this ensure it is not casted into a double
    printf("\nnum_layers = %d = %s\n",conf_set->NUM_LAYERS,byte_to_binary(conf_set->NUM_LAYERS)); //this ensure it is not casted into a double
    for (i=num_dim-1;i>=0;i--){
        aa=init_share_d0[i];
        opp->init_share[i] = aa;
        fp2bin(aa,binString);
        //fprintf(fid_tb,"%s",binString);
        //printf("init_share[%d] = %.4f = %x =%s\n",i,aa,*(unsigned int*)&(aa),binString); //this ensure it is not casted into a double
        printf("init_share[%d] = %.4f =",i,aa);
        prinf_float_type_hex(" ",aa);
        printf("=%s\n",binString);
    }
    printf("\n");
    fprintf(fid_tb,"\n");
    for (i=0;i<num_dim;i++){
        aa=conf_set->strike_prob_vect[i];
         opp->strike[i] = aa;
       fp2bin(aa,binString);
        fprintf(fid_tb,"%s",binString);
        // printf("strike[%d] = %.4f = %x=%s\n",i,aa,*(unsigned int*)&(aa),binString); //this ensure it is not casted into a double
        printf("strike[%d] = %.4f =",i,aa);
        prinf_float_type_hex(" ",aa);
        printf("=%s\n",binString);  
    }
    printf("\n");
    fprintf(fid_tb,"\n");
    aa=conf_set->CASH;
    opp->cash = aa;
    fp2bin(aa,binString);
    fprintf(fid_tb,"%s",binString);
    //  printf("cash = %.4f = %x=%s,",aa,*(unsigned int*)&(aa),binString); //this ensure it is not casted into a double
    printf("cash[%d] = %.4f =",i,aa);
    prinf_float_type_hex(" ",aa);
    printf("=%s\n",binString);  
    printf("\n");
    fprintf(fid_tb,"\n");
    for (i=num_dim-1;i>=0;i--){
        for (int j=num_dim-1;j>=0;j--){
            aa=conf_set->share_mult_vect[i][j+1]/share_mult_vect[i][j];
            opp->init_step[i][j]=aa;
            fp2bin(aa,binString);
            fprintf(fid_tb,"%s",binString);
            //      fprintf(fid_tb,"%x",*(unsigned int*)&(aa)); //this ensure it is not casted into a double
            //      printf("init_steps[%d][%d] = %.4f = %x=%s\n",i,j,aa,*(unsigned int*)&(aa),binString); //this ensure it is not casted into a double
            printf("init_step[%d][%d] = %.4f =",i,j,aa);
            prinf_float_type_hex(" ",aa);
            printf("=%s\n",binString);  
        }
    }
    printf("\n");
    fprintf(fid_tb,"\n");

    for (int j=num_dim-1;j>=0;j--){
        aa=1/(conf_set->share_mult_vect[j][0]);
        opp->init_step_next[j] = aa;
        fp2bin(aa,binString);
        fprintf(fid_tb,"%s",binString);
        //      printf("init_steps_next[%d] = %.4f = %x=%s\n",j,aa,*(unsigned int*)&(aa),binString); //this ensure it is not casted into a double
        printf("init_steps_next[%d] = %.4f =",i,aa);
        prinf_float_type_hex(" ",aa);
        printf("=%s\n",binString);  
    }

    printf("\n");
    fprintf(fid_tb,"\n");

    for (int j=num_dim;j>=0;j--){
        aa=(1/conf_set->EXP_RATE)*conf_set->eu_prob_vect[j];
        opp->mult_coeff[j] = aa;
        fp2bin(aa,binString);
        fprintf(fid_tb,"%s",binString);
        //      printf("mult_coeff(eu_prob,q)[%d] = %.4f = %x=%s\n",j,aa,*(unsigned int*)&(aa),binString); //this ensure it is not casted into a double
        printf("mult_coeff(eu_prob,q)[%d] = %.4f =",i,aa);
        prinf_float_type_hex(" ",aa);
        printf("=%s\n",binString);  
    }

    printf("\n");
    fprintf(fid_tb,"\n");

    for (int j=num_dim-1;j>=0;j--){
        aa=1/(conf_set->share_mult_vect[j][0]);
        opp->shares_coeff[j] = aa;
        fp2bin(aa,binString);
        fprintf(fid_tb,"%s",binString);
        //      printf("shares_coeff(eu_prob,q)[%d] = %.4f = %x=%s\n",j,aa,*(unsigned int*)&(aa),binString); //this ensure it is not casted into a double
        printf("shares_coeff(eu_prob,q)[%d] = %.4f =",i,aa);
        prinf_float_type_hex(" ",aa);
        printf("=%s\n",binString);  
    }

    printf("\n");
    fprintf(fid_tb,"\n");
    fclose(fid_tb);
	printf("HERE\n");

    //exit(EXIT_FAILURE);
#endif

#ifdef INIT_TB_GEN
    FILE *fid_tb=NULL;
    float aa;
    char binString[34];
    fid_tb=fopen("InitDataset.txt","w");
    fprintf(fid_tb,"%d ",((conf_set->MAX_MIN)+1)%2);
    fprintf(fid_tb,"%s ",byte_to_binary(conf_set->NUM_LAYERS)); //this ensure it is not casted into a double
    printf("\nnum_layers = %d = %s\n",conf_set->NUM_LAYERS,byte_to_binary(conf_set->NUM_LAYERS)); //this ensure it is not casted into a double
    for (i=num_dim-1;i>=0;i--){
        aa=init_share_d0[i];
        fp2bin(aa,binString);
        fprintf(fid_tb,"%s",binString);
        //    fprintf(fid_tb,"%x",*(unsigned int*)&(aa)); //Print in hex this ensure it is not casted into a double
        printf("init_share[%d] = %.4f = %x =%s\n",i,aa,*(unsigned int*)&(aa),binString); //this ensure it is not casted into a double
    }
    printf("\n");
    fprintf(fid_tb," ");
    for (i=0;i<num_dim;i++){
        aa=conf_set->strike_prob_vect[i];
        fp2bin(aa,binString);
        fprintf(fid_tb,"%s",binString);
        //    fprintf(fid_tb,"%x",*(unsigned int*)&(aa)); //this ensure it is not casted into a double
        printf("strike[%d] = %.4f = %x=%s\n",i,aa,*(unsigned int*)&(aa),binString); //this ensure it is not casted into a double
    }
    printf("\n");
    fprintf(fid_tb," ");
    aa=conf_set->CASH;
    fp2bin(aa,binString);
    fprintf(fid_tb,"%s",binString);
    //fprintf(fid_tb,"%x",*(unsigned int*)&(aa)); //this ensure it is not casted into a double
    printf("cash = %.4f = %x=%s,",aa,*(unsigned int*)&(aa),binString); //this ensure it is not casted into a double
    printf("\n");
    fprintf(fid_tb," ");
    for (i=num_dim-1;i>=0;i--){
        for (int j=num_dim-1;j>=0;j--){
            aa=conf_set->share_mult_vect[i][j+1]/share_mult_vect[i][j];
            fp2bin(aa,binString);
            fprintf(fid_tb,"%s",binString);
            //      fprintf(fid_tb,"%x",*(unsigned int*)&(aa)); //this ensure it is not casted into a double
            printf("init_steps[%d][%d] = %.4f = %x=%s\n",i,j,aa,*(unsigned int*)&(aa),binString); //this ensure it is not casted into a double
        }
    }
    printf("\n");
    fprintf(fid_tb," ");

    for (int j=num_dim-1;j>=0;j--){
        aa=1/(conf_set->share_mult_vect[j][0]);
        fp2bin(aa,binString);
        fprintf(fid_tb,"%s",binString);
        //      fprintf(fid_tb,"%x",*(unsigned int*)&(aa)); //this ensure it is not casted into a double
        printf("init_steps_next[%d] = %.4f = %x=%s\n",j,aa,*(unsigned int*)&(aa),binString); //this ensure it is not casted into a double
    }

    printf("\n");
    fprintf(fid_tb,"\n");


    printf("----------------------------------------------");
    while (fifo_size(fifo_partis)!=0){
        init_partition=(ushort *)fifo_pop(fifo_partis);
        print_partition(init_partition,num_dim,"init");
        printf("\n");
        for(i=num_dim;i>=0;i--){
            printf("init_part=%s,", byte_to_binary(init_partition[i]));
            fprintf(fid_tb,"%s",byte_to_binary(init_partition[i]));
        }

        fprintf(fid_tb," ");
        printf("\n");
        aa=*((float_type*)fifo_pop(fifo_value_vect[0]));

        fp2bin(aa,binString);
        fprintf(fid_tb,"%s",binString);
        //      fprintf(fid_tb,"%x",*(unsigned int*)&(aa)); //this ensure it is not casted into a double
        printf("val = %.4f = %x = ,s\n",aa,*(unsigned int*)&(aa),binString); //this ensure it is not casted into a double
        fprintf(fid_tb," ");

        float *share=NULL;
        share=(float_type*)fifo_pop(fifo_shares);
        print_shares(share,num_dim,"init_tb");   
        printf("\n");
        for(i=0;i<num_dim;i++){
            aa=share[i];

            fp2bin(aa,binString);
            fprintf(fid_tb,"%s",binString);
            //     fprintf(fid_tb,"%x",*(unsigned int*)&(aa)); //this ensure it is not casted into a double
            printf("share = %g  = %x=%s\n",aa,*(unsigned int*)&(aa),binString); //this ensure it is not casted into a double

        }

        fprintf(fid_tb,"\n");
    }
    fclose(fid_tb);

    exit(EXIT_FAILURE);
#endif
    ///////////////////////////////////////////////////////
    //The following destroy the fifos hence the EXIT FAILURE
    ///////////////////////////////////////////////////////
    /*#ifdef DEBUG
      while (fifo_size(fifo_partis)!=0){
      init_partition=(ushort *)fifo_pop(fifo_partis);
      print_partition(init_partition,num_dim,"init");
      printf("-");
      for (i=0; i<num_dim+1;i++){
      printf("\n v%d=%g",i,*((float_type*)fifo_pop(fifo_value_vect[i])));
      }
      printf("+");
      print_shares((float_type*)fifo_pop(fifo_shares),num_dim,"test");

      }
      exit(EXIT_FAILURE);
#endif
    /**/
}
void gen_partitions_rec(ushort* partition,  ushort pos, float_type *share,generic_fifo *fifo,generic_fifo **fifo_value_vect, generic_fifo *fifo_shares,config_set *conf_set){

    ushort *current_partition=NULL;
    ushort *new_partition=NULL;
    float_type value_opt_curr=0;
    float_type *share_curr=0;
    float_type *value_opt_curr_vect=NULL;
    int i=0;
    ushort num_dim=conf_set->NUM_DIM;
    float_type **share_mult_vect=conf_set->share_mult_vect;

    current_partition=(ushort*)calloc((num_dim+1),sizeof(ushort));
    assert_mem(current_partition);
    for (i=0;i<(num_dim+1);i++){
        current_partition[i]=partition[i];
    }
    // memcpy((void *)new_partition,(void *)current_partition,2*(num_dim));

    if (pos==(num_dim)){return;};
    while (current_partition[pos]!=0){
        new_partition=(ushort*)calloc((num_dim+1),sizeof(ushort));
        assert_mem(new_partition);
        for (i=0;i<(num_dim+1);i++){
            new_partition[i]=current_partition[i];
        }
        //need to copy to avoid using pointers that get modified
        //new partition values
        new_partition[pos]--;
        new_partition[pos+1]++;
        share_curr=(float_type *)calloc(num_dim,sizeof(float_type));
        assert_mem(share_curr);
        //compute the share at this point
        for(i=0;i<num_dim;i++){
            share_curr[i]=share[i]/share_mult_vect[i][pos]*share_mult_vect[i][pos+1];   
        }
#ifdef DBG_VHDL
        float aa,bb=0;
        if(new_partition[0]!=0){
            for(i=0;i<num_dim;i++){
                aa=share[i];
                bb=share_mult_vect[i][pos+1]/share_mult_vect[i][pos];
                printf("\nshare[%d]=%.4f=%x fact[%d]=%.4f=%x",i,share[i],*(unsigned int*)&(aa),i,share_mult_vect[i][pos+1]/share_mult_vect[i][pos],*(unsigned int*)&(bb));   
                aa=share_curr[i];
                printf("\nnew_share[%d]=%.4f=%x ",i,aa,*(unsigned int*)&(aa));   
            }
            print_partition(new_partition,num_dim,"new");
        }
#endif 
        share=share_curr;
        if(new_partition[0]!=0){
#ifdef DEBUG
            print_shares(share,num_dim,"DBG_RECUR");
            print_shares(share_curr,num_dim,"DBG_RECUR CURR");
            print_partition(new_partition,num_dim,"new");
            printf("\n");
#endif
            fifo_push(fifo_shares,(void*)share_curr);

        }
        //compute the value at this poitn 
        value_opt_curr=compute_payoff(share_curr,conf_set); 
#ifdef DBG_VHDL

        if(new_partition[0]!=0){
            aa=value_opt_curr;
            printf("\nval=%.18f=%x ",aa,*(unsigned int*)&(aa));  
        }
#endif
        //put the same value in all the fifos
        //Need to replicate it to avoid pointers problems
        value_opt_curr_vect=(float_type *)malloc((num_dim+1)*sizeof(float_type));
        assert_mem(value_opt_curr_vect);
        for (int i=0;i<num_dim+1;i++){
            value_opt_curr_vect[i]=value_opt_curr;
            if(new_partition[i]!=0){
                fifo_push(fifo_value_vect[i],(void *)&(value_opt_curr_vect[i]));
            }
        }
#ifdef DEBUG
        prinf_float_type(" val",value_opt_curr,PRNT_PREC);
        printf("\n");
        printf("Final layer partitions=[");
        for (i=0;i<num_dim+1;i++){
            printf("%d,",new_partition[i]);
        }
        printf("]\n");
#endif
        //push partitions    
        //fifo_push(fifo,(void*)new_partition); //this computes all partition on final layer
        if (new_partition[0]!=0){ //This way only the partition of the prev layer are stored
            fifo_push(fifo,(void*)new_partition);
        }
        gen_partitions_rec(new_partition,pos+1,share_curr,fifo,fifo_value_vect,fifo_shares,conf_set);
        current_partition=new_partition;
    }


}

void print_partition(ushort *partition,ushort num_dim, char *heading){
    printf("\n %s part=",heading);
    for (int i=0;i<num_dim+1;i++){
        printf("%u,",partition[i]);
    }
}
void print_shares(float_type *shares,ushort num_dim, char *heading){
    char tmp[20];
    char tmp2[20];
    for (int i=0;i<num_dim;i++){
        strcpy(tmp2,heading);
        sprintf(tmp,"\n %s  shar[%d] ",tmp2,i);
        prinf_float_type(tmp,shares[i],PRNT_PREC);
        //    printf("%g,",shares[i]);
    }
}

float_type  compute_value(float_type *old_val, float_type *shares,config_set *conf_set){

    float_type r=conf_set->EXP_RATE;
    ushort num_dim=conf_set->NUM_DIM;
    float_type eu_val=0;
    float_type *eu_prob_vect=conf_set->eu_prob_vect;
    float_type payoff,max_val=0;
    //European part
#ifdef DEBUG
    float_type aa;
    aa=(1/r);
    prinf_float_type("\n++1/r",aa,PRNT_PREC);
    prinf_float_type_hex("--> HEX",aa);
    //printf("\n++1/r=%.18f-->HEX=%x",aa,*(unsigned int*)&(aa));   
#endif
    for(int i=0;i<num_dim+1;i++){
        eu_val+=(1/r)*eu_prob_vect[i]*old_val[i];    
#ifdef DEBUG
        char tmp[20];    
        aa=(1/r)*eu_prob_vect[i];
        sprintf(tmp,"\n++val_coef[%d]",i);
        prinf_float_type(tmp,aa,PRNT_PREC);
        prinf_float_type_hex("--> HEX",aa);
        //     printf("\n++val_coef[%d]=%.25f-->HEX=%x",i,aa,*(unsigned int*)&(aa));   
#endif
    }
    if (conf_set->OPT_AMERICAN==1){
        payoff=compute_payoff(shares,conf_set);
        max_val=(float_type)MAX_FUNC(payoff,eu_val);
        return  max_val;

    }else{
        return  eu_val;
    }
}

float_type compute_payoff(float_type *shares,config_set *conf_set){
    float_type *strike_prob_vect=conf_set->strike_prob_vect;
    ushort max_min=conf_set->MAX_MIN;
    ushort num_dim=conf_set->NUM_DIM;
    float_type cash=conf_set->CASH;
    float_type *s_star=NULL;
    float_type f_tmp,max_val=0;

    payoff_num_calls++;
    s_star=(float_type *)calloc(num_dim,sizeof(float_type));
    assert_mem(s_star);
    if(conf_set->CALL_PUT==1){ //call option
        for (int i=0;i<num_dim;i++){
            s_star[i]=shares[i]-strike_prob_vect[i];
        }
        if (max_min==1){
            f_tmp=max_array(s_star,num_dim);
        }else{
            f_tmp=min_array(s_star,num_dim);
        }

    }else{ //put option
        /*old*/
        for (int i=0;i<num_dim;i++){
            s_star[i]=strike_prob_vect[i]-shares[i];
        }
        if (max_min==0){
            f_tmp=max_array(s_star,num_dim);
        }else{
            f_tmp=min_array(s_star,num_dim);
        }
        /**/
        /*new* //gives identicall sol
          if (max_min==1){
          f_tmp=max_array(shares,num_dim);
          }else{
          f_tmp=min_array(shares,num_dim);
          }
          f_tmp=strike_prob_vect[0]-f_tmp;
        /**/
    }

    max_val=(float_type)MAX_FUNC(f_tmp,(float_type)cash);

    free(s_star);
    if (max_val!=0){ payoff_num_nz++;};

#ifdef PLOT_CONSTELLATION
    //  fprintf(fid_cost,"%.6f,%.6f,%.6f\n",shares[0],shares[1],max_val);
    fprintf_float_type(fid_cost,"",shares[0],8);
    fprintf_float_type(fid_cost,",",shares[1],8);
    fprintf_float_type(fid_cost,",",max_val,8);
    fprintf(fid_cost,"\n");
#endif

    return max_val;  

    //This is only for testing Bernard cases for drift debugging

    //    max_val=(float_type)POW_FUNC((float_type)shares[0],(float_type)2.0);   //s1^2
    //  	return max_val;
    //S1*s2;
    // return (float_type)(shares[0]*shares[1]); 
    //S1
    //     return shares[0];
    //S2
    // return shares[1];

    //s1^2
    //max_val=(float_type)POW_FUNC(shares[0],(float_type)2.0);
    //return max_val;

    //Bond
    //return (double)1; //ok

    //Account
    //    max_val=pow(1+conf_set->RATE,conf_set->T);  //ok
    //    max_val=EXP_FUNC(conf_set->RATE*conf_set->T); //ok
    //    return (float_type)max_val;

    //No RATE
    //    return 40;
}


float_type max_array(float_type *array,ushort dim){
    float_type maxf=array[0];
    for(int i=0;i<dim;i++){
        if (array[i]>maxf){maxf=array[i];}
    }
    return maxf;
}

float_type min_array(float_type *array,ushort dim){
    float_type minf=array[0];
    for(int i=0;i<dim;i++){
        if (array[i]<minf){minf=array[i];}
    }
    return minf;
}


const char *byte_to_binary(int x)
{
    static char b[6];
    b[0] = '\0';

    int z;
    for (z = 32; z > 0; z >>= 1)
    {
        strcat(b, ((x & z) == z) ? "1" : "0");
    }

    return b;
}
void compute_d_and_q(float_type **sigma_matrix,float_type **d_matrix,float_type *q_vector, float_type rate, float_type dt,ushort num_dim,ulong T){

    float_type  exp_rate=EXP_FUNC((float_type)rate*dt);
    float_type *mu=NULL;
    ushort i,j=0;
    float_type **chol_matrix=NULL;
    float_type **m_matrix=NULL;
    float_type **l_matrix=NULL;
    float_type **a_matrix=NULL;
    float_type **b_matrix=NULL;

    chol_matrix=(float_type **)malloc(num_dim*sizeof(float_type *));
    assert_mem(chol_matrix);
    for(i=0;i<num_dim;i++){
        chol_matrix[i]=(float_type *)malloc(num_dim*sizeof(float_type ));
        assert_mem(chol_matrix[i]);
    }

    //  m_matrix=gsl_matrix_calloc(num_dim,num_dim+1);
    m_matrix=(float_type **)malloc(num_dim*sizeof(float_type *));
    assert_mem(m_matrix);
    for(i=0;i<num_dim;i++){
        m_matrix[i]=(float_type *)malloc((num_dim+1)*sizeof(float_type ));
        assert_mem(m_matrix[i]);
    }

    l_matrix=(float_type **)malloc(num_dim*sizeof(float_type *));
    assert_mem(l_matrix);
    for(i=0;i<num_dim;i++){
        l_matrix[i]=(float_type *)malloc(num_dim*sizeof(float_type ));
        assert_mem(l_matrix[i]);
    }

    //  a_matrix=gsl_matrix_calloc(num_dim,num_dim+1);
    a_matrix=(float_type **)malloc(num_dim*sizeof(float_type *));
    assert_mem(a_matrix);
    for(i=0;i<num_dim;i++){
        a_matrix[i]=(float_type *)malloc((num_dim+1)*sizeof(float_type ));
        assert_mem(a_matrix[i]);
    }

    //  b_matrix=gsl_matrix_calloc(num_dim+1,num_dim+1);
    b_matrix=(float_type **)malloc((num_dim+1)*sizeof(float_type *));
    assert_mem(b_matrix);
    for(i=0;i<num_dim+1;i++){
        b_matrix[i]=(float_type *)malloc((num_dim+1)*sizeof(float_type ));
        assert_mem(b_matrix[i]);
    }

    //MU calculation
    mu=(float_type *)calloc(num_dim,sizeof(float_type));
    assert_mem(mu);
    for(i=0;i<num_dim;i++){
        mu[i]=rate-(float_type)0.5*sigma_matrix[i][i];
    }

    //Cholesky factorization ==>L=chol(SIGMA)
    for(i=0;i<num_dim;i++){
        for(j=0;j<num_dim;j++){
            //    gsl_matrix_memcpy(chol_matrix,sigma_matrix); ///make a copy
            chol_matrix[i][j]=sigma_matrix[i][j];
        }
    }
    linalg_cholesky_decomp_gen(chol_matrix,num_dim,num_dim); //write on top of tmp
    for (i=0;i<num_dim;i++){
        for(j=0;j<num_dim;j++){
            if(i>=j){
                l_matrix[i][j]=chol_matrix[i][j]; //get only low part
            }
        }
    }

    float_type on=-1;
    //M matrix creation
    for (i=1;i<=num_dim;i++){
        for(j=1;j<=num_dim+1;j++){
            if(i==j){
                m_matrix[i-1][j-1]=SQRT_FUNC((float_type)((num_dim-i+1)/(float_type)(num_dim-i+2)));
            }else{
                if (j>i && j<num_dim+2){
                    m_matrix[i-1][j-1]=on/SQRT_FUNC((float_type)((num_dim-i+1)*(float_type)(num_dim-i+2)));
                }
            }
        }
    }

    //directions calculation
    float_type ftmp;
    ftmp=SQRT_FUNC((float_type)((num_dim+1)*dt));//==>   b=sqrt((k+1)*dt);
    //  gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,l_matrix,m_matrix,0.0,a_matrix);//a_matrix=1.0l_matrix*m_matrix+0.0a_matrix; ==> A=L*M
    matrix_mult_gemm(a_matrix,l_matrix,m_matrix,num_dim,num_dim+1,num_dim,num_dim,num_dim,num_dim+1);
    for (i=0;i<num_dim;i++){
        for(j=0;j<num_dim+1;j++){
            d_matrix[i][j]=EXP_FUNC(ftmp*a_matrix[i][j]+mu[i]*dt);   //==>   d(j,i)=exp(b*A(j,i)+mu(j)*dt);
            b_matrix[i][j]=d_matrix[i][j];
        }
    }


    //last row of b
    for(j=0;j<num_dim+1;j++){
        b_matrix[num_dim][j]=exp_rate;
    }

    //b_vector of the lin sys
    for (i=0;i<num_dim+1;i++){    
        q_vector[i]=exp_rate;
    }
    //solve lin sys
    linalg_HH_svx_gen(b_matrix,num_dim+1,num_dim+1,q_vector,num_dim+1);  //householder solver in place (ie override q_vector)

#ifdef DEBUG_2ND_MOM
    print_float_type_matrix(d_matrix,num_dim,num_dim+1,"\nd_old=[");
    print_float_type_vector(q_vector,num_dim+1,"q_old=[");

#endif

#ifdef SCND_MOM_MATCH
    //////////////////////////
    /// second moment matching
    /////////////////////////
    float_type  **d_sq;
    float_type  **run_sum_matrix;
    float_type  **enew_matrix=NULL;

    float_type v;



    d_sq=(float_type **)malloc(num_dim*sizeof(float_type *));
    assert_mem(d_sq);
    for(i=0;i<num_dim;i++){
        d_sq[i]=(float_type *)malloc(num_dim*sizeof(float_type ));
        assert_mem(d_sq[i]);
    }


    enew_matrix=(float_type **)malloc(num_dim*sizeof(float_type *));
    assert_mem(enew_matrix);
    for(i=0;i<num_dim;i++){
        enew_matrix[i]=(float_type *)malloc(num_dim*sizeof(float_type ));
        assert_mem(enew_matrix[i]);
    }

    run_sum_matrix=(float_type **)malloc(num_dim*sizeof(float_type *));
    assert_mem(run_sum_matrix);
    for(i=0;i<num_dim;i++){
        run_sum_matrix[i]=(float_type *)malloc(num_dim*sizeof(float_type ));
        assert_mem(run_sum_matrix[i]);
    }


    //compute ==> A=(q(1)*d1*d1'+q(2)*d2*d2'+q(3)*d3*d3') genertalized as summation
    for(int dir=0;dir<num_dim+1;dir++){
        for(i=0;i<num_dim;i++){
            for(j=0;j<num_dim;j++){
                v=d_matrix[i][dir]*d_matrix[j][dir]; //d1*d1'
                d_sq[i][j]=v*q_vector[dir]; ////q(1)*d1*d1'
                run_sum_matrix[i][j]+=d_sq[i][j]; //running sum
            }
        }
    }
    //    gsl_matrix_add_constant(a_matrix,-pow(exp_rate,2));//-R^2;
    for(i=0;i<num_dim;i++){
        for(j=0;j<num_dim;j++){
            run_sum_matrix[i][j]-=POW_FUNC(exp_rate,2); ////-R^2;
        }
    }

    /*correct*/
    float_type sigma1=SQRT_FUNC(sigma_matrix[0][0]);
    float_type sigma2=SQRT_FUNC(sigma_matrix[1][1]);
    float_type corr=sigma_matrix[0][1]/sigma1/sigma2;
    float_type r_tmp=EXP_FUNC(2*rate*dt);
    float_type E11=r_tmp*(EXP_FUNC(POW_FUNC(sigma1,2)*dt)-1);	 //Enew(1,1)=exp(2*r*dt)*(exp(sigma1^2*dt)-1)
    float_type E22=r_tmp*(EXP_FUNC(POW_FUNC(sigma2,2)*dt)-1);	 //Enew(2,2)=exp(2*r*dt)*(exp(sigma2^2*dt)-1)
    float_type E12=r_tmp*(EXP_FUNC(sigma1*sigma2*corr*dt)-1); //Enew(1,2)=Enew(2,1)=exp(2*r*dt)*(exp(sigma1*sigma2*corr*dt)-1)
    enew_matrix[0][0]=E11;
    enew_matrix[1][1]=E22;
    enew_matrix[0][1]=E12;
    enew_matrix[1][0]=E12;
    /***/
    /*wrong*
      gsl_matrix_memcpy(enew_matrix,sigma_matrix); //make a copy
      gsl_matrix_scale(enew_matrix,dt);//Enew=E*dt;
    /**/

    //Cholesky factorization
    float_type **l1_matrix=NULL;
    float_type **l1_inv_matrix=NULL;
    float_type **l2_matrix=NULL;
    //   l1_matrix=  gsl_matrix_calloc(num_dim,num_dim);
    l1_matrix=(float_type **)malloc(num_dim*sizeof(float_type *));
    assert_mem(l1_matrix);
    for(i=0;i<num_dim;i++){
        l1_matrix[i]=(float_type *)malloc(num_dim*sizeof(float_type ));
        assert_mem(l1_matrix[i]);
    }
    l1_inv_matrix=(float_type **)malloc(num_dim*sizeof(float_type *));
    assert_mem(l1_inv_matrix);
    for(i=0;i<num_dim;i++){
        l1_inv_matrix[i]=(float_type *)malloc(num_dim*sizeof(float_type ));
        assert_mem(l1_inv_matrix[i]);
    }

    //   l2_matrix=  gsl_matrix_calloc(num_dim,num_dim);
    l2_matrix=(float_type **)malloc(num_dim*sizeof(float_type *));
    assert_mem(l2_matrix);
    for(i=0;i<num_dim;i++){
        l2_matrix[i]=(float_type *)malloc(num_dim*sizeof(float_type ));
        assert_mem(l2_matrix[i]);
    }
    linalg_cholesky_decomp_gen(run_sum_matrix,num_dim,num_dim); //write on top of a_matrix
    for (i=0;i<num_dim;i++){
        for(j=0;j<num_dim;j++){
            if(i>=j){
                l1_matrix[i][j]=run_sum_matrix[i][j]; //get only low part
            }
        }
    }
    linalg_cholesky_decomp_gen(enew_matrix,num_dim,num_dim); //write on top of enew_matrix
    for (i=0;i<num_dim;i++){
        for(j=0;j<num_dim;j++){
            if(i>=j){
                l2_matrix[i][j]=enew_matrix[i][j]; //get only low part
            }
        }
    }

    /*correct*/
    matrix_inv(l1_matrix,num_dim,l1_inv_matrix);
    matrix_mult_gemm(l1_matrix,l2_matrix,l1_inv_matrix,num_dim,num_dim,num_dim,num_dim,num_dim,num_dim);

    /**/
    //     gsl_matrix_add_constant(d_matrix,-exp_rate);//d-R*ones(k,k+1)
    for (i=0;i<num_dim;i++){
        for (j=0;j<num_dim+1;j++){
            d_matrix[i][j]+=-exp_rate;
        }
    }

    //  gsl_matrix_set_all (m_matrix, exp_rate);//R*ones(k,k+1)  //reuse m_matrix 
    for (i=0;i<num_dim;i++){
        for (j=0;j<num_dim+1;j++){
            m_matrix[i][j]=exp_rate;
        }
    }

    float_type **dnew_matrix;
    dnew_matrix=(float_type **)malloc(num_dim*sizeof(float_type *));
    assert_mem(dnew_matrix);
    for (i=0;i<num_dim;i++){
        dnew_matrix[i]=(float_type *)malloc((num_dim+1)*sizeof(float_type ));
        assert_mem(dnew_matrix[i]);
    }

    //     gsl_blas_dgemm (CblasNoTrans,CblasNoTrans,1.0,l2_matrix,d_matrix,1.0, m_matrix); 
    //dnew=Lnew*d_matrix+m_matrix //new risk probability measure
    matrix_mult_gemm(dnew_matrix,l1_matrix,d_matrix,num_dim,num_dim+1,num_dim,num_dim,num_dim,num_dim+1);
    for (i=0;i<num_dim;i++){
        for (j=0;j<num_dim+1;j++){
            d_matrix[i][j]=dnew_matrix[i][j]+m_matrix[i][j];   
            b_matrix[i][j]=d_matrix[i][j]; //    gsl_matrix_memcpy(d_matrix,m_matrix);
        }
    }

    //last row of b
    for(j=0;j<num_dim+1;j++){
        b_matrix[num_dim][j]=exp_rate;
    }

    //b_vector of the lin sys
    for (i=0;i<num_dim+1;i++){
        q_vector[i]=exp_rate;
    }
    //solve lin sys

    //////////////////////////////////////////////
    /**/
    ///////////////////////////////////////////////
    linalg_HH_svx_gen(b_matrix,num_dim+1,num_dim+1,q_vector,num_dim+1);  //householder solver in place (ie override q_vector)


    for (i=0;i<num_dim;i++){
        free(l2_matrix[i]); 
        free(l1_matrix[i]); 
        free(run_sum_matrix[i]);
        free(l1_inv_matrix[i]);
        free(enew_matrix[i]);
        free(d_sq[i]);
        free(a_matrix[i]);
        free(l_matrix[i]);
        free(m_matrix[i]);
        free(chol_matrix[i]);
    }
    for(i=0;i<num_dim+1;i++){
        free(b_matrix[i]);
    }
    free(b_matrix);
    free(a_matrix);
    free(l_matrix);
    free(m_matrix);
    free(chol_matrix);
#endif

    free(mu);

}


void read_config(FILE *fid,config_set *conf_set, char *config_file){
    char line[STR_MAX]				= {""};
    char rubbish[STR_MAX]			= {""};
    char temp_string[STR_MAX]		= {""};
    char variable_name[STR_MAX]		= {""};
    char variable_value[STR_MAX]	= {""};
    int i,j,num_arg						= 0;

    size_t string_size				= 0;
    ushort VERBOSE		= 0;
    ushort OPT_EUROPEAN=0;
    ushort CALL_PUT=0;
    ushort OPT_AMERICAN=0;
    ushort NUM_LAYERS=0;
    ushort NUM_DIM=0;
    ushort MAX_MIN=0;

    float_type CASH=0;
    float_type RATE=0;
    float_type EXP_RATE=0;
    float_type T=0;

    float_type **share_mult_vect=NULL;
    float_type *eu_prob_vect=NULL;
    float_type *strike_prob_vect=NULL;
    float_type *init_share_value_vect=NULL;
    float_type **sigma_matrix=NULL;

    /* Read config file line-by-line */
    while(!feof(fid)) {
        /* read line */
        fgets(temp_string, STR_MAX, fid);

        /* remove comments */
        string_size = strcspn(temp_string,"#");
        strncpy(line, temp_string, string_size);
        line[string_size] = '\0';

        /* remove leading spaces */
        string_size = strspn(line, " \t\n");	/* length of leading whitespaces */
        strcpy(temp_string, &line[string_size]);

        /* read variable name and value */
        num_arg = sscanf(temp_string,"%[^ \t]%[ \t]%[^ \t\n]",variable_name,rubbish,variable_value);
        if(num_arg == 0 || num_arg == EOF) {
            continue;
        } else if(num_arg != 3) {
            fprintf(stderr, "\nERROR: Failed to read properly from %s file.\n",config_file);
            exit(EXIT_FAILURE);
        }

        /* Large if else structure */
        if( !strcmp(variable_name,"OPT_EUROPEAN") ) {
            OPT_EUROPEAN = (ushort)atol(variable_value);
        } else if( !strcmp(variable_name,"CALL_PUT") ) {
            CALL_PUT = (ushort)atol(variable_value);
        } else if( !strcmp(variable_name,"OPT_AMERICAN") ) {
            OPT_AMERICAN = (ushort)atol(variable_value);
        } else if( !strcmp(variable_name,"NUM_LAYERS") ) {
            NUM_LAYERS   = (ulong)atol(variable_value);
        } else if( !strcmp(variable_name,"NUM_DIM") ) {
            NUM_DIM      = (ushort)atol(variable_value);
            //Alloc vector for shares coeff etc
            share_mult_vect = (float_type **)malloc(NUM_DIM*sizeof(float_type *));
            assert_mem(share_mult_vect);
            for (i=0;i<NUM_DIM;i++){
                share_mult_vect[i] = (float_type *)malloc((NUM_DIM+1)*sizeof(float_type));	
                assert_mem(share_mult_vect[i]);
            }
            eu_prob_vect = (float_type *)calloc(NUM_DIM+1,sizeof(float_type));
            assert_mem(eu_prob_vect);
            strike_prob_vect = (float_type *)calloc(NUM_DIM,sizeof(float_type));
            assert_mem(strike_prob_vect);
            init_share_value_vect = (float_type *)calloc(NUM_DIM,sizeof(float_type));
            assert_mem(init_share_value_vect);
            //      sigma_matrix=gsl_matrix_calloc(NUM_DIM,NUM_DIM);
            sigma_matrix=(float_type **)malloc(NUM_DIM*sizeof(float_type *));
            assert_mem(sigma_matrix);
            for (i=0;i<NUM_DIM;i++){
                sigma_matrix[i]=(float_type *)malloc(NUM_DIM*sizeof(float_type));
                assert_mem(sigma_matrix[i]);
            }




        } else if( !strcmp(variable_name,"SIGMA") ) {
            for (i=0;i<NUM_DIM;i++){		    
                for (j=0;j<NUM_DIM;j++){		    
                    num_arg = sscanf(variable_value,"%[^ ,]%[,]%[^ \t\n]",temp_string,rubbish,variable_value);	   
                    if(num_arg == 3 || num_arg == 1 && i==NUM_DIM-1) {
                        sigma_matrix[i][j]=((float_type)floor(atof(temp_string)*10000)/10000);  //this make a differnece for the bad cas
                       printf("real sigma_matrix[%d][%d] = %f\n",sigma_matrix[i][j]);

                    }else{
                        fprintf(stderr, "\nERROR: Failed to read SIGMA properly from %s file.\n",config_file);
                        exit(EXIT_FAILURE);
                    } 
                }
            }
        } else if( !strcmp(variable_name,"INIT_SHARE_VALUE") ) {
            for (i=0;i<NUM_DIM;i++){
                num_arg = sscanf(variable_value,"%[^ ,]%[,]%[^ \t\n]",temp_string,rubbish,variable_value);	   
                if(num_arg == 3 || num_arg == 1 && i==NUM_DIM-1) {
                    init_share_value_vect[i]=(float_type)atof(temp_string);
                }else{
                    fprintf(stderr, "\nERROR: Failed to read PROB properly from %s file.\n",config_file);
                    exit(EXIT_FAILURE);
                } 
            }

        } else if( !strcmp(variable_name,"T") ) {
            T      = (float_type)atof(variable_value);

        } else if( !strcmp(variable_name,"RATE") ) {
            RATE      = (float_type)atof(variable_value);

        } else if( !strcmp(variable_name,"STRIKE") ) {
            for (i=0;i<NUM_DIM;i++){
                num_arg = sscanf(variable_value,"%[^ ,]%[,]%[^ \t\n]",temp_string,rubbish,variable_value);	   
                if(num_arg == 3 || num_arg == 1 && i==NUM_DIM-1) {
                    printf("string before atof = %s and double %f\n",temp_string,atof(temp_string));
                    strike_prob_vect[i]=atof(temp_string);
                    printf("first value = %f\n",strike_prob_vect[i]);
                }else{
                    fprintf(stderr, "\nERROR: Failed to read STRIKE properly from %s file.\n",config_file);
                    exit(EXIT_FAILURE);
                } 
            }



        } else if( !strcmp(variable_name,"MAX_MIN") ) {
            MAX_MIN      = (ushort)atol(variable_value);

        } else if( !strcmp(variable_name,"CASH") ) {
            CASH      = (float_type)atof(variable_value);

        } else if( !strcmp(variable_name,"VERBOSE") ) {
            VERBOSE      = (ushort)atol(variable_value);

        } else {
            fprintf(stderr, "\nERROR: Unknown configuration option \"%s\"\n",variable_name);
            exit(EXIT_FAILURE);
        }


    }	/* while */

    EXP_RATE=EXP_FUNC(RATE*(T/NUM_LAYERS));
    //  EXP_RATE=pow(1+RATE,T/NUM_LAYERS);

    conf_set->OPT_EUROPEAN=OPT_EUROPEAN;	
    conf_set->OPT_AMERICAN=OPT_AMERICAN;	  
    conf_set->CALL_PUT=CALL_PUT;	
    conf_set->T=T;	     
    conf_set->NUM_LAYERS=NUM_LAYERS;	     
    conf_set->NUM_DIM=NUM_DIM;	     
    conf_set->MAX_MIN=MAX_MIN;	     
    conf_set->RATE=RATE;	     
    conf_set->EXP_RATE=EXP_RATE;	     
    conf_set->CASH=CASH;	     
    conf_set->VERBOSE=VERBOSE;	     
    conf_set->share_mult_vect=share_mult_vect; 
    conf_set->eu_prob_vect=eu_prob_vect;	  
    conf_set->strike_prob_vect=strike_prob_vect;	     
    printf("value = %f\n",conf_set->strike_prob_vect[0]);
    conf_set->init_share_value_vect=init_share_value_vect;
    conf_set->sigma_matrix=sigma_matrix;
    if(conf_set->VERBOSE){
        printf("T	= %.4f\n",conf_set->T	    );
        printf("NUM_LAYERS	= %u\n",conf_set->NUM_LAYERS	    );
        print_float_type_matrix(sigma_matrix,NUM_DIM,NUM_DIM,"\nSIGMA=[");
        printf("\nOPT_EUROPEAN    = %u\n",conf_set->OPT_EUROPEAN   );
        printf("OPT_AMERICAN	= %u\n",conf_set->OPT_AMERICAN   );
        printf("CALL_PUT	= %u\n",conf_set->CALL_PUT   );
        printf("NUM_LAYERS	= %u\n",conf_set->NUM_LAYERS	    );
        printf("NUM_DIM		= %u\n",conf_set->NUM_DIM	    );
        printf("RATE		= %.4f\n",conf_set->RATE	    );
        printf("EXP_RATE		= %.4f\n",conf_set->EXP_RATE	    );
        printf("MAX_MIN		= %u\n",conf_set->MAX_MIN	    );
        printf("CASH		= %.4f\n",conf_set->CASH	    );	
        printf("VERBOSE		= %u",conf_set->VERBOSE	    ); 	
        printf("\nSTRIKE	=");
        for(i=0;i<NUM_DIM;i++){
            printf("%.4f,",conf_set->strike_prob_vect[i]);
        }
        printf("\nINIT_SHARE_VALUE	 =");
        for(i=0;i<NUM_DIM;i++){
            printf("%.4f,",conf_set->init_share_value_vect[i]);
        }
    }
}
void  prinf_float_type(char *head,float_type val,ushort frac_digits){
    char str[80];
    char tmp[10];
    sprintf(str,"%s",head);
    strcat (str,"%.");
    sprintf(tmp,"%d",frac_digits);
    strcat (str,tmp);
    strcat (str,PRNT_FRM);
    printf(str,val); 
}
void  fprintf_float_type(FILE *fid,char *head,float_type val,ushort frac_digits){
    char str[80];
    char tmp[10];
    sprintf(str,"%s",head);
    strcat (str,"%.");
    sprintf(tmp,"%d",frac_digits);
    strcat (str,tmp);
    strcat (str,PRNT_FRM);
    fprintf(fid,str,val); 
}

void prinf_float_type_hex(char *head,float_type val){
    char str[80];
    union {
        float_type f;
        char c[sizeof(float_type)]; // Edit: changed to this
    } prnt_union;
    prnt_union.f=val;
    sprintf(str,"%s",head);
    //  strcat (str," =");
    printf(str); 
    for ( int i = sizeof(float_type)-1; i>=0 ; --i ) printf( "%02X" , prnt_union.c[i] & 0x00FF );
}



void print_gsl_matrix(gsl_matrix *m){
    ulong r=m->size1;
    ulong c=m->size2;

    printf("\n");
    for(int i=0;i<r;i++){
        printf("\t");
        for(int j=0;j<c;j++){
            printf("%.18f,",gsl_matrix_get(m,i,j));
        }
        printf(";\n");
    }
}
void print_gsl_vector(gsl_vector *v){
    ulong r=v->size;

    for(int i=0;i<r;i++){
        printf("%.18f,",gsl_vector_get(v,i));
    }
    printf("]\n");

}

void print_float_type_matrix(float_type  **m,const ulong row,const  ulong col, char *head){
    ulong r=row;
    ulong c=col;

    printf("%s=",head);
    for(int i=0;i<r;i++){
        printf("\t");
        for(int j=0;j<c;j++){
            prinf_float_type(", ",m[i][j],PRNT_PREC);;
        }
        printf(";\n");
    }
}
void print_float_type_vector(float_type *v,ulong dim,char * head){
    ulong r=dim;
    printf("%s",head);
    for(int i=0;i<r;i++){
        prinf_float_type(", ",v[i],PRNT_PREC);;
    }
    printf("]\n");
}

void convert_to_gls_mat(float_type **m,ulong rows,ulong cols,gsl_matrix *m_gsl){
    for(int i=0;i<rows;i++){
        for(int j=0;j<cols;j++){
            gsl_matrix_set(m_gsl,i,j,(double)m[i][j]);
        }
    }
}
